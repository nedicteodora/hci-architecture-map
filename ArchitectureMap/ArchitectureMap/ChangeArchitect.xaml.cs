﻿using ArchitectureMap.model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArchitectureMap
{
    /// <summary>
    /// Interaction logic for ChangeArchitect.xaml
    /// </summary>
    public partial class ChangeArchitect : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string input)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(input));
            }
        }
        private string labelField;
        public string LabelField
        {
            get { return labelField; }
            set
            {
                labelField = value;
                OnPropertyChanged("LabelField");
            }
        }
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        private DateTime birthYear;
        public DateTime BirthYear
        {
            get { return birthYear; }
            set
            {
                birthYear = value;
                OnPropertyChanged("BirthYear");
            }
        }
        private DateTime deathYear;
        public DateTime DeathYear
        {
            get { return deathYear; }
            set
            {
                deathYear = value;
                OnPropertyChanged("DeathYear");
            }
        }
        private string birthPlace;
        public string BirthPlace
        {
            get { return birthPlace; }
            set
            {
                birthPlace = value;
                OnPropertyChanged("BirthPlace");
            }
        }
        private string nationality;
        public string Nationality
        {
            get { return nationality; }
            set
            {
                nationality = value;
                OnPropertyChanged("Nationality");
            }
        }
        private Database Base { get; set; }
        // putanja do slike

        private string image;

        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }

        }
        public Architect SelectedArch { get; set; }

        public ChangeArchitect(Architect a)
        {
            SelectedArch = a;
            InitializeComponent();
            Base = new Database();

            this.DataContext = this;

            LabelField = a.Label;
            FirstName = a.FirstName;
            LastName = a.LastName;
            BirthYear = a.Born;
            DeathYear = a.Died;
            BirthPlace = a.BirthPlace;
            Nationality = a.Nationality;
            Image = a.Image;
            if(a.Image.Contains("http"))
                urlBox.Text = a.Image;
        }

        public Thickness DocumentMargin
        {
            get { return (Thickness)DataContext; }
            set { DataContext = value; }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            DialogResult = false;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't accept the dialog box if there is invalid data
            //if (!IsValid(this)) return;
            // checkLabel();
            if (urlBox.Text != "" && urlBox.Text != "Insert url...")
                image = urlBox.Text;
            else
            {
                if (image == "")
                {
                    image = "https://w0.pngwave.com/png/451/619/architecture-interior-design-services-computer-icons-design-png-clip-art.png";
                }
            }

            //Console.WriteLine("Ime: " + firstName + "Prezime: " + LastName + "Mesto rodjenja: " + birthPlace + "Godina rodjenja: " + birthYear + "Godina: " + deathYear + "Nacionalnost" + nationality);
            if (firstName == "" || lastName == "" || birthYear == null || birthPlace == "" || nationality == "" || image == "")
            {
                System.Windows.MessageBox.Show("Invalid data");
                return;
            }
            // sve je ok, dodaj i sacuvaj
            Architect newArch = new Architect(labelField, firstName, lastName, birthYear, deathYear, birthPlace, nationality, image, SelectedArch.X1, SelectedArch.Y1, SelectedArch.X2, SelectedArch.Y2, SelectedArch.X3
                , SelectedArch.Y3, SelectedArch.X4, SelectedArch.Y4);
            int index = 0;
            foreach (Architect a in Base.architects)
            {
                if (a.Label == newArch.Label)
                {
                    break;
                }
                index++;
            }
            Base.architects.RemoveAt(index);
            Base.architects.Add(newArch);
            Base.SaveArchitect();
            System.Windows.MessageBox.Show("Saved");
            this.Close();

            // Dialog box accepted
            //DialogResult = true;

        }
    /*
        private bool CheckLabel()
        {
            if (labelField == "")
                return false;
            foreach (Architect a in Base.architects)
            {
                if (a.Label == labelField)
                {
                    return false;
                }
            }

            return true;
        }
*/
       

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            //  OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.Show();

            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Choose icon";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                // ikonica.Source = new BitmapImage(new Uri(fileDialog.FileName));
                //slika = ikonica.Source.ToString();
                image = fileDialog.FileName;
            }


        }

        private void AddTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "")
            {
                urlBox.Text = "Insert url...";
                urlBox.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "Insert url...")
            {
                urlBox.Text = "";
                urlBox.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void CommandBinding_Executed1(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = HelpProvider.GetHelpKey((DependencyObject)f);
                HelpProvider.ShowHelp(key, this);
            }
        }
    }
}
