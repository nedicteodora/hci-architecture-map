﻿using ArchitectureMap.model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArchitectureMap
{
    /// <summary>
    /// Interaction logic for DialogBoxArchitect.xaml
    /// </summary>
    public partial class DialogBoxArchitect : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string input)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(input));
            }
        }
        private string labelField;
        public string LabelField
        {
            get { return labelField; }
            set
            {
                labelField = value;
                OnPropertyChanged("LabelField");
            }
        }
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set
            {
                firstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        private DateTime birthYear;
        public DateTime BirthYear
        {
            get { return birthYear; }
            set
            {
                birthYear = value;
                OnPropertyChanged("BirthYear");
            }
        }
        private DateTime deathYear;
        public DateTime DeathYear
        {
            get { return deathYear; }
            set
            {
                deathYear = value;
                OnPropertyChanged("DeathYear");
            }
        }
        private string birthPlace;
        public string BirthPlace
        {
            get { return birthPlace; }
            set
            {
                birthPlace = value;
                OnPropertyChanged("BirthPlace");
            }
        }
        private string nationality;
        public string Nationality
        {
            get { return nationality; }
            set
            {
                nationality = value;
                OnPropertyChanged("Nationality");

            }
        }
        private Database Base { get; set; }
        // putanja do slike

        private string image;

        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }

        }


        public DialogBoxArchitect()
        {
            InitializeComponent();
            Base = new Database();

            firstName = "";
            lastName = "";
            birthPlace = "";
            nationality = "";
            image = "";
            urlBox.Text = "";


            this.DataContext = this;

        }
    
        public Thickness DocumentMargin
        {
            get { return (Thickness)DataContext; }
            set { DataContext = value; }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            DialogResult = false;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't accept the dialog box if there is invalid data
            //if (!IsValid(this)) return;
            // checkLabel();
            if(!CheckLabel())
            {
                System.Windows.MessageBox.Show("Label already exists");
                return;
            }
            if (urlBox.Text != "" && urlBox.Text != "Insert url...")
                image = urlBox.Text;
            else
            {
                if (image == "")
                {
                    image = "https://w0.pngwave.com/png/451/619/architecture-interior-design-services-computer-icons-design-png-clip-art.png";
                }
            }


            //Console.WriteLine("Ime: " + firstName + "Prezime: " + LastName + "Mesto rodjenja: " + birthPlace + "Godina rodjenja: " + birthYear + "Godina: " + deathYear + "Nacionalnost" + nationality);
            if (firstName == "" || lastName == "" || birthPlace == "" || nationality == "" || BirthYear.ToString("yyyy").Equals("0001"))
            {
                System.Windows.MessageBox.Show("Invalid data");
                return;
            }
            // sve je ok, dodaj i sacuvaj
            Architect newArch = new Architect(labelField, firstName, lastName, birthYear, deathYear, birthPlace, nationality, image);
            Base.architects.Add(newArch);
            Base.SaveArchitect();
            System.Windows.MessageBox.Show("Saved");
            this.Close();
            // Dialog box accepted
            //DialogResult = true;

        }

        private bool CheckLabel()
        {
            if (labelField == "")
                return false;
            foreach(Architect a in Base.architects)
            {
                if(a.Label == labelField)
                {
                    return false;
                }
            }
        
            return true;
        }

        // Validate all dependency objects in a window
        private bool IsValid(DependencyObject node)
        {
            // Check if dependency object was passed
            if (node != null)
            {
                // Check if dependency object is valid.
                // NOTE: Validation.GetHasError works for controls that have validation rules attached 
                var isValid = !Validation.GetHasError(node);
                if (!isValid)
                {
                    // If the dependency object is invalid, and it can receive the focus,
                    // set the focus
                    if (node is IInputElement) Keyboard.Focus((IInputElement)node);
                    return false;
                }
            }

            // If this dependency object is valid, check all child dependency objects
            return LogicalTreeHelper.GetChildren(node).OfType<DependencyObject>().All(IsValid);

            // All dependency objects are valid
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            //  OpenFileDialog openFileDialog = new OpenFileDialog();
            //openFileDialog.Show();
            
            OpenFileDialog fileDialog = new OpenFileDialog();

        
            fileDialog.Title = "Choose icon";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
               // ikonica.Source = new BitmapImage(new Uri(fileDialog.FileName));
                //slika = ikonica.Source.ToString();
                image = fileDialog.FileName;
            }
           

        }

        private void AddTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "")
            {
                urlBox.Text = "Insert url...";
                urlBox.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "Insert url...")
            {
                urlBox.Text = "";
                urlBox.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void CommandBinding_Executed3(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = HelpProvider.GetHelpKey((DependencyObject)f);
                HelpProvider.ShowHelp(key, this);
            }
        }
    }
}
