﻿using ArchitectureMap.model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArchitectureMap
{
    /// <summary>
    /// Interaction logic for DialogBoxArchitectWorks.xaml
    /// </summary>

    public partial class DialogBoxArchitectWorks : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string input)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(input));
            }
        }
        private string labelField;
        public string LabelField
        {
            get { return labelField; }
            set
            {
                labelField = value;
                OnPropertyChanged("LabelField");
            }
        }

        private string workName;
        public string WorkName
        {
            get { return workName; }
            set
            {
                workName = value;
                OnPropertyChanged("WorkName");
            }
        }
        private string place;
        public string Place
        {
            get { return place; }
            set
            {
                place = value;
                OnPropertyChanged("Place");
            }
        }
        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }
        private string year;
        public string Year
        {
            get { return year; }
            set
            {
                year = value;
                OnPropertyChanged("Year");
            }
        }

        private string purpose;
        public string Purpose
        {
            get { return purpose; }
            set
            {
                purpose = value;
                OnPropertyChanged("Purpose");
            }
        }

        private string image;
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }

        }


        public ObservableCollection<string> Epoch
        {
            get;
            set;
        }

        public ObservableCollection<string> Architects
        {
            get;
            set;
        }

        private Database Base { get; set; }

        public DialogBoxArchitectWorks()
        {
            InitializeComponent();
            InitializeEpoch();
            Base = new Database();
            InitializeArchitects();

            image = "";
            workName = "";
            place = "";
            description = "";
            year = "";
            purpose = "";
            urlBox.Text = "";

            this.DataContext = this;
        }

        public void InitializeEpoch()
        {
            Epoch = new ObservableCollection<string>();
            Epoch.Add(model.Epoch.antiquity.ToString());
            Epoch.Add(model.Epoch.baroque.ToString());
            Epoch.Add(model.Epoch.byzantium.ToString());
            Epoch.Add(model.Epoch.classicism.ToString());
            Epoch.Add(model.Epoch.early_christian.ToString());
            Epoch.Add(model.Epoch.gothic.ToString());
            Epoch.Add(model.Epoch.mesopotamian.ToString());
            Epoch.Add(model.Epoch.modern.ToString());
            Epoch.Add(model.Epoch.postmodern.ToString());
            Epoch.Add(model.Epoch.prehistoric.ToString());
            Epoch.Add(model.Epoch.renaissance.ToString());
            Epoch.Add(model.Epoch.romance.ToString());
        }

        public void InitializeArchitects()
        {
            Architects = new ObservableCollection<string>();
            foreach (Architect a in Base.architects)
            {
                Architects.Add(a.FirstName + " " + a.LastName);
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            DialogResult = false;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't accept the dialog box if there is invalid data
            //if (!IsValid(this)) return;

            bool WorldWonder = false;
            bool UnderProtection = false;
            if (WWYes.IsChecked == true)
                WorldWonder = true;
            if (UPYes.IsChecked == true)
                UnderProtection = true;

            if (!CheckLabel())
            {
                System.Windows.MessageBox.Show("Label already exists");
                return;
            }
            if (urlBox.Text != ""&& urlBox.Text != "Insert url...")
                image = urlBox.Text;
            else
            {
                if (image == "")
                {
                    image = "https://f1.pngfuel.com/png/956/1007/729/real-estate-building-office-biurowiec-commercial-building-architecture-icon-design-symbol-png-clip-art.png";
                }
            }

            if (workName == "" || place == "" || description == "" || year == "" || purpose == "")
            {
                System.Windows.MessageBox.Show("Invalid data");
                return;
            }


            //sve ok sacuvaj
            ArchitecturalWork newArch = new ArchitecturalWork(labelField, workName, place, description, year, architect_comboBox.SelectedValue.ToString(), FindEpoch(), WorldWonder, UnderProtection, purpose, image, FindArchitectLabel());
            Base.architecturalWorks.Add(newArch);
            Base.SaveArchitectWorks();
            System.Windows.MessageBox.Show("Saved");
            this.Close();
            // Dialog box accepted
            // DialogResult = true;
        }

        public Epoch FindEpoch()
        {
            if (epoch_comboBox.SelectedValue.Equals("prehistoric"))
                return model.Epoch.prehistoric;
            if (epoch_comboBox.SelectedValue.Equals("mesopotamian"))
                return model.Epoch.mesopotamian;
            if (epoch_comboBox.SelectedValue.Equals("antiquity"))
                return model.Epoch.antiquity;
            if (epoch_comboBox.SelectedValue.Equals("byzantium"))
                return model.Epoch.byzantium;
            if (epoch_comboBox.SelectedValue.Equals("early_christian"))
                return model.Epoch.early_christian;
            if (epoch_comboBox.SelectedValue.Equals("romance"))
                return model.Epoch.romance;
            if (epoch_comboBox.SelectedValue.Equals("gothic"))
                return model.Epoch.gothic;
            if (epoch_comboBox.SelectedValue.Equals("renaissance"))
                return model.Epoch.renaissance;
            if (epoch_comboBox.SelectedValue.Equals("baroque"))
                return model.Epoch.baroque;
            if (epoch_comboBox.SelectedValue.Equals("classicism"))
                return model.Epoch.classicism;
            if (epoch_comboBox.SelectedValue.Equals("modern"))
                return model.Epoch.modern;
            else
                return model.Epoch.postmodern;
        }

        public string FindArchitectLabel()
        {
            foreach (Architect a in Base.architects)
            {
                if (architect_comboBox.SelectedValue.ToString().Equals(a.FirstName + " " + a.LastName))
                    return a.Label;
            }
            return "";

        }

        private bool CheckLabel()
        {
            if (labelField == "")
                return false;
            foreach (ArchitecturalWork a in Base.architecturalWorks)
            {
                if (a.Label == labelField)
                {
                    return false;
                }
            }

            return true;
        }

        // Validate all dependency objects in a window
        private bool IsValid(DependencyObject node)
        {
            // Check if dependency object was passed
            if (node != null)
            {
                // Check if dependency object is valid.
                // NOTE: Validation.GetHasError works for controls that have validation rules attached 
                var isValid = !Validation.GetHasError(node);
                if (!isValid)
                {
                    // If the dependency object is invalid, and it can receive the focus,
                    // set the focus
                    if (node is IInputElement) Keyboard.Focus((IInputElement)node);
                    return false;
                }
            }

            // If this dependency object is valid, check all child dependency objects
            return LogicalTreeHelper.GetChildren(node).OfType<DependencyObject>().All(IsValid);

            // All dependency objects are valid
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Choose icon";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                // ikonica.Source = new BitmapImage(new Uri(fileDialog.FileName));
                //slika = ikonica.Source.ToString();
                image = fileDialog.FileName;
            }
        }

        private void AddTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "")
            {
                urlBox.Text = "Insert url...";
                urlBox.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "Insert url...")
            {
                urlBox.Text = "";
                urlBox.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void CommandBinding_Executed4(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = HelpProvider.GetHelpKey((DependencyObject)f);
                HelpProvider.ShowHelp(key, this);
            }
        }
    }      
}

