﻿using ArchitectureMap.model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ArchitectureMap
{
    /// <summary>
    /// Interaction logic for ChangeArchitectWork.xaml
    /// </summary>
    public partial class ChangeArchitectWork : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string input)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(input));
            }
        }
        private string labelField;
        public string LabelField
        {
            get { return labelField; }
            set
            {
                labelField = value;
                OnPropertyChanged("LabelField");
            }
        }

        private string workName;
        public string WorkName
        {
            get { return workName; }
            set
            {
                workName = value;
                OnPropertyChanged("WorkName");
            }
        }
        private string place;
        public string Place
        {
            get { return place; }
            set
            {
                place = value;
                OnPropertyChanged("Place");
            }
        }
        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }
        private string year;
        public string Year
        {
            get { return year; }
            set
            {
                year = value;
                OnPropertyChanged("Year");
            }
        }

        public ObservableCollection<string> Architects
        {
            get;
            set;
        }



        private string purpose;
        public string Purpose
        {
            get { return purpose; }
            set
            {
                purpose = value;
                OnPropertyChanged("Purpose");
            }
        }

        private string image;
        public string Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }

        }

        public ObservableCollection<string> Epoch
        {
            get;
            set;
        }

        private Database Base { get; set; }
        private string selectedEpoch;
        public string SelectedEpoch
        {
            get { return selectedEpoch; }
            set
            {
                if (value != selectedEpoch)
                {
                    selectedEpoch = value;
                    OnPropertyChanged("SelectedEpoch");
                }
            }
        }
        private bool wwYesChecked;
        public bool WWYesChecked
        {
            get { return wwYesChecked; }
            set
            {
                if (value != wwYesChecked)
                {
                    wwYesChecked = value;
                    OnPropertyChanged("WWYesChecked");
                }
            }
        }
        private bool wwNoChecked;
        public bool WWNoChecked
        {
            get { return wwNoChecked; }
            set
            {
                if (value != wwNoChecked)
                {
                    wwNoChecked = value;
                    OnPropertyChanged("WWNoChecked");
                }
            }
        }
        private bool upYesChecked;
        public bool UPYesChecked
        {
            get { return upYesChecked; }
            set
            {
                if (value != upYesChecked)
                {
                    upYesChecked = value;
                    OnPropertyChanged("UPYesChecked");
                }
            }
        }
        private bool upNoChecked;
        public bool UPNoChecked
        {
            get { return this.upNoChecked; }
            set
            {
              
                    upNoChecked = value;
                    OnPropertyChanged("UPNoChecked");
                
            }
        }
        public ArchitecturalWork Selected_arc_work { get; set; }

        public ChangeArchitectWork(ArchitecturalWork aw)
        {
            Selected_arc_work = aw;
            InitializeComponent();
            InitializeEpoch();
            
            Base = new Database();
            InitializeArchitects();

            this.DataContext = this;
            LabelField = aw.Label;
            WorkName = aw.Name;
            Place = aw.Place;
            Description = aw.Description;
            Year = aw.Year;
            architect_comboBox.SelectedValue = aw.Architect;
            Purpose = aw.Purpose;
            if (aw.WorldWonder)
            {
                wwYesChecked = true;
                wwNoChecked = false;
            }
            else
            {
                wwYesChecked = false;
                wwNoChecked = true;
            }

            if (aw.UnderProtection)
            {
                upYesChecked = true;
                upNoChecked = false;
            }
            else
            {
                upYesChecked = false;
                upNoChecked = true;
            }
            SelectedEpoch = aw.Epoch.ToString();
            Image = aw.Image;
            if (aw.Image.Contains("http"))
                urlBox.Text = aw.Image;

        }
        public void InitializeArchitects()
        {
            Architects = new ObservableCollection<string>();
            foreach (Architect a in Base.architects)
            {
                Architects.Add(a.FirstName + " " + a.LastName);
            }

        }
        public void InitializeEpoch()
        {
            Epoch = new ObservableCollection<string>();
            Epoch.Add(model.Epoch.antiquity.ToString());
            Epoch.Add(model.Epoch.baroque.ToString());
            Epoch.Add(model.Epoch.byzantium.ToString());
            Epoch.Add(model.Epoch.classicism.ToString());
            Epoch.Add(model.Epoch.early_christian.ToString());
            Epoch.Add(model.Epoch.gothic.ToString());
            Epoch.Add(model.Epoch.mesopotamian.ToString());
            Epoch.Add(model.Epoch.modern.ToString());
            Epoch.Add(model.Epoch.postmodern.ToString());
            Epoch.Add(model.Epoch.prehistoric.ToString());
            Epoch.Add(model.Epoch.renaissance.ToString());
            Epoch.Add(model.Epoch.romance.ToString());
        }

        public Thickness DocumentMargin
        {
            get { return (Thickness)DataContext; }
            set { DataContext = value; }
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            // Dialog box canceled
            //DialogResult = false;
            this.Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            // Don't accept the dialog box if there is invalid data
            //if (!IsValid(this)) return;

            bool WorldWonder = false;
            bool UnderProtection = false;
            if (WWYes.IsChecked == true)
                WorldWonder = true;
            if (UPYes.IsChecked == true)
                UnderProtection = true;

            if (urlBox.Text != "" && urlBox.Text != "Insert url...")
                image = urlBox.Text;
            else
            {
                if (image == "")
                {
                    image = "https://f1.pngfuel.com/png/956/1007/729/real-estate-building-office-biurowiec-commercial-building-architecture-icon-design-symbol-png-clip-art.png";
                }
            }

            if (workName == "" || place == "" || description == "" || year == "" || purpose == "" || image == "")
            {
                System.Windows.MessageBox.Show("Invalid data");
                return;
            }


            //sve ok sacuvaj
            ArchitecturalWork newArch = new ArchitecturalWork(labelField, workName, place, description, year, architect_comboBox.SelectedValue.ToString(), FindEpoch(), WorldWonder, UnderProtection, purpose, image, Selected_arc_work.X1, Selected_arc_work.Y1, Selected_arc_work.X2, Selected_arc_work.Y2, Selected_arc_work.X3, Selected_arc_work.Y3, Selected_arc_work.X4,Selected_arc_work.Y4, FindArchitectLabel());
            // pronadji je u bazi
            int index = 0;
            foreach(ArchitecturalWork a in Base.architecturalWorks)
            {
                if(a.Label == newArch.Label)
                {
                    break;
                }
                index++;
            }
            Base.architecturalWorks.RemoveAt(index);
            Base.architecturalWorks.Add(newArch);
            Base.SaveArchitectWorks();
            System.Windows.MessageBox.Show("Saved");
            this.Close();
            // Dialog box accepted
            // DialogResult = true;
        }
        public string FindArchitectLabel()
        {
            foreach (Architect a in Base.architects)
            {
                if (architect_comboBox.SelectedValue.ToString().Equals(a.FirstName + " " + a.LastName))
                    return a.Label;
            }
            return "";

        }
        public Epoch FindEpoch()
        {
            if (epoch_comboBox.SelectedValue.Equals("prehistoric"))
                return model.Epoch.prehistoric;
            if (epoch_comboBox.SelectedValue.Equals("mesopotamian"))
                return model.Epoch.mesopotamian;
            if (epoch_comboBox.SelectedValue.Equals("antiquity"))
                return model.Epoch.antiquity;
            if (epoch_comboBox.SelectedValue.Equals("byzantium"))
                return model.Epoch.byzantium;
            if (epoch_comboBox.SelectedValue.Equals("early_christian"))
                return model.Epoch.early_christian;
            if (epoch_comboBox.SelectedValue.Equals("romance"))
                return model.Epoch.romance;
            if (epoch_comboBox.SelectedValue.Equals("gothic"))
                return model.Epoch.gothic;
            if (epoch_comboBox.SelectedValue.Equals("renaissance"))
                return model.Epoch.renaissance;
            if (epoch_comboBox.SelectedValue.Equals("baroque"))
                return model.Epoch.baroque;
            if (epoch_comboBox.SelectedValue.Equals("classicism"))
                return model.Epoch.classicism;
            if (epoch_comboBox.SelectedValue.Equals("modern"))
                return model.Epoch.modern;
            else
                return model.Epoch.postmodern;
        }
/*
        private bool CheckLabel()
        {
            if (labelField == "")
                return false;
            foreach (ArchitecturalWork a in Base.architecturalWorks)
            {
                if (a.Label == labelField)
                {
                    return false;
                }
            }

            return true;
        }

 */      

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Choose icon";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                // ikonica.Source = new BitmapImage(new Uri(fileDialog.FileName));
                //slika = ikonica.Source.ToString();
                image = fileDialog.FileName;
            }
        }

        private void AddTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "")
            {
                urlBox.Text = "Insert url...";
                urlBox.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextUrl(object sender, RoutedEventArgs e)
        {
            if (urlBox.Text == "Insert url...")
            {
                urlBox.Text = "";
                urlBox.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void CommandBinding_Executed2(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = HelpProvider.GetHelpKey((DependencyObject)f);
                HelpProvider.ShowHelp(key, this);
            }
        }
    }
}
