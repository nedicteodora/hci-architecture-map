﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ArchitectureMap
{
    public class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                DateTime myDate = (DateTime)value;
                if (myDate != DateTime.MinValue)
                {
                    return myDate.ToString("dd.MM.yyyy.");
                }
            }

            return "         /";
        } 

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public object ConvertInCs(object value)
        {
            if (value != null)
            {
                DateTime myDate = (DateTime)value;
                if (myDate != DateTime.MinValue)
                {
                    return myDate.ToString("dd/MM/yyyy");
                }
            }

            return "/";
        }
    }



    public class BoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool val = (bool)value;
                if (val == true)
                {
                    return "Yes";
                }
            }

            return "No";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
