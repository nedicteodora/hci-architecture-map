﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchitectureMap.model
{
    public class ArchitecturalWork
    {
        public string Label { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public string Description { get; set; }
        public string Year { get; set; }
       // public DateTime Year { get; set; }
        public string Architect { get; set; }
        public Epoch Epoch { get; set; }
        public bool WorldWonder { get; set; }
        public bool UnderProtection { get; set; }
        public string Purpose { get; set; }
        public string Image { get; set; }
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
        public double X3 { get; set; }
        public double Y3 { get; set; }
        public double X4 { get; set; }
        public double Y4 { get; set; }
        public string ArchitectLabel { get; set; } // sluzi za povezivanje slicica na mapi

        public ArchitecturalWork()
        {}
        public ArchitecturalWork(string l, string n, string p, string d, string year, string a, Epoch e, bool ww, bool up, string pp, string i, double x, double y, double x2, double y2,
             double x3, double y3, double x4, double y4, string al)
        {
            Label = l;
            Name = n;
            Place = p;
            Description = d;
            Year = year;
            Architect = a;
            Epoch = e;
            WorldWonder = ww;
            UnderProtection = up;
            Purpose = pp;
            Image = i;
            X1 = x;
            Y1 = y;
            X2 = x2;
            Y2 = y2;
            X3 = x3;
            Y3 = y3;
            X4 = x4;
            Y4 = y4;
            ArchitectLabel = al; 

        }

        public ArchitecturalWork(string l, string n, string p, string d, string year, string a, Epoch e, bool ww, bool up, string pp, string i, double x, double y, double x2, double y2,
             double x3, double y3, double x4, double y4)
        {
            Label = l;
            Name = n;
            Place = p;
            Description = d;
            Year = year;
            Architect = a;
            Epoch = e;
            WorldWonder = ww;
            UnderProtection = up;
            Purpose = pp;
            Image = i;
            X1 = x;
            Y1 = y;
            X2 = x2;
            Y2 = y2;
            X3 = x3;
            Y3 = y3;
            X4 = x4;
            Y4 = y4;
        }

            public ArchitecturalWork(string l, string n, string p, string d, string year, string a, Epoch e, bool ww, bool up, string pp, string im, string al)
        {
            Label = l;
            Name = n;
            Place = p;
            Description = d;
            Year = year;
            Architect = a;
            Epoch = e;
            WorldWonder = ww;
            UnderProtection = up;
            Purpose = pp;
            Image = im;
            ArchitectLabel = al;
            X1 = -1;
            Y1 = -1;
            X2 = -1;
            Y2 = -1;
            X3 = -1;
            Y3 = -1;
            X4 = -1;
            Y4 = -1;

        }

        public ArchitecturalWork(string l, string n, string p, string d, string year, string a, Epoch e, bool ww, bool up, string pp, string im)
        {
            Label = l;
            Name = n;
            Place = p;
            Description = d;
            Year = year;
            Architect = a;
            Epoch = e;
            WorldWonder = ww;
            UnderProtection = up;
            Purpose = pp;
            Image = im;
            X1 = -1;
            Y1 = -1;
            X2 = -1;
            Y2 = -1;
            X3 = -1;
            Y3 = -1;
            X4 = -1;
            Y4 = -1;
        }
    }
}
