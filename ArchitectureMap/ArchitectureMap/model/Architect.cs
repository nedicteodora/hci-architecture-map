﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ArchitectureMap.model
{
    public class Architect
    {
      

        public string Label { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Born { get; set; }
        public DateTime Died { get; set; }
        public string BirthPlace { get; set; }
        public string Nationality { get; set; }
        public string Image { get; set; }
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
        public double X3 { get; set; }
        public double Y3 { get; set; }
        public double X4 { get; set; }
        public double Y4 { get; set; }

        public Architect() {}
        public Architect(string l, string fn, string ln, DateTime b, DateTime d, string bp, string n, string i, double x, double y, double x2, double y2, double x3, double y3, double x4, double y4)
        {
            Label = l;
            FirstName = fn;
            LastName = ln;
            Born = b;
            Died = d;
            BirthPlace = bp;
            Nationality = n;
            Image = i;
            X1 = x;
            Y1 = y;
            X2 = x2;
            Y2 = y2;
            X3 = x3;
            Y3 = y3;
            X4 = x4;
            Y4 = y4;
        }

        public Architect(string l, string fn, string ln, DateTime b, string bp, string n, string i, double x, double y, double x2, double y2, double x3, double y3, double x4, double y4)
        {
            Label = l;
            FirstName = fn;
            LastName = ln;
            Born = b;
            BirthPlace = bp;
            Nationality = n;
            Image = i;
            X1 = x;
            Y1 = y;
            X2 = x2;
            Y2 = y2;
            X3 = x3;
            Y3 = y3;
            X4 = x4;
            Y4 = y4;
        }

        public Architect(string label, string firstName, string lastName, DateTime birthYear, DateTime deathYear, string birthPlace, string nationality, string im)
        {
            Label = label;
            FirstName = firstName;
            LastName = lastName;
            Born = birthYear;
            Died = deathYear;
            BirthPlace = birthPlace;
            Nationality = nationality;
            Image = im;
            X1 = -1;
            Y1 = -1;
            X2 = -1;
            Y2 = -1;
            X3 = -1;
            Y3 = -1;
            X4 = -1;
            Y4 = -1;
        }
    }
}
