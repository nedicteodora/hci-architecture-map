﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArchitectureMap.model
{
    public class Database
    {
        public ObservableCollection<Architect> architects = new ObservableCollection<Architect>();
        public ObservableCollection<ArchitecturalWork> architecturalWorks = new ObservableCollection<ArchitecturalWork>();

        private string pathArchitects = @"architects.txt";
        private string pathArchitecturalWorks = @"architectural_works.txt";

        public Database()
        {
            loadArchitects();
            loadArchitecturalWorks();

        }

        private void loadArchitects()
        {
            if (File.Exists(pathArchitects))
            {

                using (StreamReader reader = File.OpenText(pathArchitects))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    architects = (ObservableCollection<Architect>)serializer.Deserialize(reader, typeof(ObservableCollection<Architect>));
                }
            }
            else
            {
                architects = new ObservableCollection<Architect>();
            }
        }

        private void loadArchitecturalWorks()
        {
            if (File.Exists(pathArchitecturalWorks))
            {

                using (StreamReader reader = File.OpenText(pathArchitecturalWorks))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    architecturalWorks = (ObservableCollection<ArchitecturalWork>)serializer.Deserialize(reader, typeof(ObservableCollection<ArchitecturalWork>));
                }
            }
            else
            {
                architecturalWorks = new ObservableCollection<ArchitecturalWork>();
            }
        }

        public void UpdateBase()
        {
            loadArchitects();
            loadArchitecturalWorks();
        }

        public void SaveArchitect()
        {
            using (StreamWriter writer = File.CreateText(pathArchitects))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, architects);
                writer.Close();
            }


        }

        public void SaveArchitectWorks()
        {
            using (StreamWriter writer = File.CreateText(pathArchitecturalWorks))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, architecturalWorks);
                writer.Close();
            }
        }
    }
}
