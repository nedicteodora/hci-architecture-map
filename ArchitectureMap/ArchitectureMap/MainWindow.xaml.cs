﻿using ArchitectureMap.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ArchitectureMap
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {

        private DateConverter dc = new DateConverter();
        private ObservableCollection<Architect> architectList = new ObservableCollection<Architect>();
        private ObservableCollection<ArchitecturalWork> worksList = new ObservableCollection<ArchitecturalWork>();
        private Point startPoint;
        private Database db;

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string v)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(v));
            }
        }
        public ObservableCollection<ArchitecturalWork> ArchitectWorks
        {
            get { return worksList; }
            set
            {
                if (worksList != value)
                {

                    worksList = value;
                    OnPropertyChanged("ArchitectWorks");
                }
            }
        }
        public ObservableCollection<Architect> ArchitectMaps
        {
            get { return architectList; }
            set
            {
                if (architectList != value)
                {

                    architectList = value;
                    OnPropertyChanged("ArchitectMaps");
                }
            }
        }
        public ObservableCollection<string> Epoch
        {
            get;
            set;
        }

        public ObservableCollection<string> Nationalities
        {
            get;
            set;
        }

        private void LoadAll()
        {
            foreach (Architect a in db.architects)
            {
                bool result = false;
                foreach (var c in canvas.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                        {
                            result = true;
                        }
                    }
                }
                if (result)
                    continue;
                if (a.X1 != -1 || a.Y1 != -1)
                {
                    Image img = new Image();
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Label;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label +"\n"+"Name: " + a.FirstName +" "+ a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                  
                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck1);
                    btn.Tag = a.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;

                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove;
                                       
                    canvas.Children.Add(img);
                    canvas.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X1 + 20);
                    Canvas.SetTop(btn, a.Y1 - 30);

                    Canvas.SetLeft(img, a.X1 - 20);
                    Canvas.SetTop(img, a.Y1 - 20);
                }
                bool result2 = false;
                foreach (var c in canvas1.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                        {
                            result2 = true;
                        }
                    }
                }

                if (result2)
                    continue;
                if (a.X2 != -1 || a.Y2 != -1)
                {
                    Image img = new Image();
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }


                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Label;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck2);
                    btn.Tag = a.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;

                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove1;

                    canvas1.Children.Add(img);
                    canvas1.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X2 + 20);
                    Canvas.SetTop(btn, a.Y2 - 30);
                    Canvas.SetLeft(img, a.X2 - 20);
                    Canvas.SetTop(img, a.Y2 - 20);
                }

                bool result3 = false;
                foreach (var c in canvas2.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                        {
                            result3 = true;
                        }
                    }
                }

                if (result3)
                    continue;
                if (a.X3 != -1 || a.Y3 != -1)
                {
                    Image img = new Image();
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }


                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Label;
                    WrapPanel wp = new WrapPanel();

                    wp.Orientation = Orientation.Vertical;
                    
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck3);
                    btn.Tag = a.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove2;

                    canvas2.Children.Add(img);
                    canvas2.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X3 + 20);
                    Canvas.SetTop(btn, a.Y3 - 30);
                    Canvas.SetLeft(img, a.X3 - 20);
                    Canvas.SetTop(img, a.Y3 - 20);
                }
                bool result4 = false;
                foreach (var c in canvas3.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                        {
                            result4 = true;
                        }
                    }
                }

                if (result4)
                    continue;
                if (a.X4 != -1 || a.Y4 != -1)
                {
                    Image img = new Image();
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }


                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Label;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck4);
                    btn.Tag = a.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;

                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove3;

                    canvas3.Children.Add(img);
                    canvas3.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X4 + 20);
                    Canvas.SetTop(btn, a.Y4 - 30);
                    Canvas.SetLeft(img, a.X4 - 20);
                    Canvas.SetTop(img, a.Y4 - 20);
                }
            }
        }


        private void LoadAllWorks()
        {
            foreach (ArchitecturalWork a in db.architecturalWorks)
            {
                bool result = false;
                foreach (var c in canvas.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                        {
                            result = true;
                        }
                    }
                }
                if (result)
                    continue;
                if (a.X1 != -1 || a.Y1 != -1)
                {
                    Image img = new Image();
                    if (a.Image != null)
                    {
                        try
                        {
                            if (!a.Image.Equals(""))
                                img.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                        
                       
                    }
                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Name;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                        + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;
                

                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));
                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck5);
                    btn.Tag = a.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;

                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove5;

                    canvas.Children.Add(img);
                    canvas.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X1 + 20);
                    Canvas.SetTop(btn, a.Y1 - 30);

                
                    Canvas.SetLeft(img, a.X1 - 20);
                    Canvas.SetTop(img, a.Y1 - 20);

                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(a.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = a.X1 - 20;
                                l.Y1 = a.Y1 - 20;
                                l.X2 = Canvas.GetLeft((Image)i);
                                l.Y2 = Canvas.GetTop((Image)i);
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = a.ArchitectLabel+" "+a.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas.Children.Add(l);
                    }
                }

                bool result2 = false;
                foreach (var c in canvas1.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                        {
                            result2 = true;
                        }
                    }
                }
                if (result2)
                    continue;
                if (a.X2 != -1 || a.Y2 != -1)
                {
                    Image img = new Image();
                    if (a.Image != null)
                    {
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }

                    }
                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Name;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                       + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;


                    wp.Children.Add(label);


                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck6);
                    btn.Tag = a.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove6;

                    canvas1.Children.Add(img);
                    canvas1.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X2 + 20);
                    Canvas.SetTop(btn, a.Y2 - 30);
                    Canvas.SetLeft(img, a.X2 - 20);
                    Canvas.SetTop(img, a.Y2 - 20);

                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas1.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(a.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = a.X2 - 20;
                                l.Y1 = a.Y2 - 20;
                                l.X2 = Canvas.GetLeft((Image)i);
                                l.Y2 = Canvas.GetTop((Image)i);
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = a.ArchitectLabel+" "+a.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas1.Children.Add(l);
                    }
                }

                bool result3 = false;
                foreach (var c in canvas2.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                        {
                            result3 = true;
                        }
                    }
                }
                if (result3)
                    continue;
                if (a.X3 != -1 || a.Y3 != -1)
                {
                    Image img = new Image();
                    if (a.Image != null)
                    {
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }

                    }
                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Name;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                       + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;


                    wp.Children.Add(label);
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck7);
                    btn.Tag = a.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove7;

                    canvas2.Children.Add(img);
                    canvas2.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X3 + 20);
                    Canvas.SetTop(btn, a.Y3 - 30);
                    Canvas.SetLeft(img, a.X3 - 20);
                    Canvas.SetTop(img, a.Y3 - 20);

                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas2.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(a.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = a.X3 - 20;
                                l.Y1 = a.Y3 - 20;
                                l.X2 = Canvas.GetLeft((Image)i);
                                l.Y2 = Canvas.GetTop((Image)i);
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = a.ArchitectLabel + " " + a.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas2.Children.Add(l);
                    }
                }

                bool result4 = false;
                foreach (var c in canvas3.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                        {
                            result4 = true;
                        }
                    }
                }
                if (result4)
                    continue;
                if (a.X4 != -1 || a.Y4 != -1)
                {
                    Image img = new Image();
                    if (a.Image != null)
                    {
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }

                    }
                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = a.Name;
                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                       + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;


                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;


                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck8);
                    btn.Tag = a.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove8;

                    canvas3.Children.Add(img);
                    canvas3.Children.Add(btn);
                    Canvas.SetLeft(btn, a.X4 + 20);
                    Canvas.SetTop(btn, a.Y4 - 30);
                    Canvas.SetLeft(img, a.X4 - 20);
                    Canvas.SetTop(img, a.Y4 - 20);
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas3.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(a.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = a.X4 - 20;
                                l.Y1 = a.Y4 - 20;
                                l.X2 = Canvas.GetLeft((Image)i);
                                l.Y2 = Canvas.GetTop((Image)i);
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = a.ArchitectLabel + " " + a.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas3.Children.Add(l);
                    }
                }
            }
        }

        public MainWindow()
        {
            this.WindowState = System.Windows.WindowState.Maximized;
            // load data from files
            db = new Database();

            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;

            InitializeComponent();

            this.Scrolllll.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2;
            this.Scrolllll1.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2;
            this.Scrolllll2.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2;
            this.Scrolllll3.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2;
            this.Stack1.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2 - 40;
            this.Stack2.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2 - 40;
            this.tabControl2.Width = System.Windows.SystemParameters.PrimaryScreenWidth / 2 ;
            Thickness margin = StackPanelFilter1.Margin;
            margin.Left = 100 ;
            StackPanelFilter1.Margin = margin;
            Thickness margin2 = StackPanelFilter2.Margin;
            margin2.Left = 100;
            StackPanelFilter2.Margin = margin2;



            this.DataContext = this;
            LoadAll();
            LoadAllWorks();
            architectList = db.architects;
            worksList = db.architecturalWorks;
            InitializeEpoch();
            InitializeNationalities();


            Console.WriteLine("Welcome");
        }
        //Nationalities

        public void InitializeEpoch()
        {
            Epoch = new ObservableCollection<string>();
            Epoch.Add(model.Epoch.antiquity.ToString());
            Epoch.Add(model.Epoch.baroque.ToString());
            Epoch.Add(model.Epoch.byzantium.ToString());
            Epoch.Add(model.Epoch.classicism.ToString());
            Epoch.Add(model.Epoch.early_christian.ToString());
            Epoch.Add(model.Epoch.gothic.ToString());
            Epoch.Add(model.Epoch.mesopotamian.ToString());
            Epoch.Add(model.Epoch.modern.ToString());
            Epoch.Add(model.Epoch.postmodern.ToString());
            Epoch.Add(model.Epoch.prehistoric.ToString());
            Epoch.Add(model.Epoch.renaissance.ToString());
            Epoch.Add(model.Epoch.romance.ToString());
        }

        public void InitializeNationalities()
        {
            Nationalities = new ObservableCollection<string>();
            List<string> nat = new List<string>();
            foreach(Architect a in db.architects)
            {
                if (!nat.Contains(a.Nationality))
                    nat.Add(a.Nationality);
            }
            foreach (string n in nat)
                Nationalities.Add(n);
        }

        private void NewArchitect(object sender, RoutedEventArgs e)
        {
            DialogBoxArchitect dd = new DialogBoxArchitect();
            dd.ShowDialog();

            db.UpdateBase();
            this.ArchitectMaps = db.architects;
        }

        private void NewArchitectWork(object sender, RoutedEventArgs e)
        {
            DialogBoxArchitectWorks d2 = new DialogBoxArchitectWorks();
            d2.ShowDialog();

            db.UpdateBase();
            this.ArchitectWorks = db.architecturalWorks;
        }

        private void Izmeni_arh_work(object sender, RoutedEventArgs e)
        {

            if (dataGrid2.SelectedValue == null)
            {
                System.Windows.MessageBox.Show("There's no selected architectual work");
                
                return;
            }
            if (dataGrid2.SelectedValue is ArchitecturalWork)
            {
                ArchitecturalWork aw = (ArchitecturalWork)dataGrid2.SelectedValue;
                // otvori prozor za izmenu arh dela
                // otvori prozor za izmenu arh dela
                var s = new ChangeArchitectWork(aw);
                s.ShowDialog();
                //Console.WriteLine("Updatujem bazu arh_work");
                db.UpdateBase();
                this.ArchitectWorks = db.architecturalWorks;


            }
        }

        private void Izmeni_arhitektu(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedValue == null)
            {
                System.Windows.MessageBox.Show("There's no selected architect");
                return;
            }
            if (dataGrid1.SelectedValue is Architect)
            {
                Architect m = (Architect)dataGrid1.SelectedValue;
                // otvori prozor za izmenu arhitekte
                var s = new ChangeArchitect(m);
                s.ShowDialog();

                db.UpdateBase();
                this.ArchitectMaps = db.architects;



            }
        }

        private void Obrisi_arhitektu(object sender, RoutedEventArgs e)
        {
            if (dataGrid1.SelectedValue == null)
            {
                System.Windows.MessageBox.Show("There's no selected architect");
                return;
            }
            if (dataGrid1.SelectedValue is Architect)
            {
                Architect m = (Architect)dataGrid1.SelectedValue;
                // upit
                MessageBoxResult result = System.Windows.MessageBox.Show("Are you sure you want to delete architect?", "Delete architect", MessageBoxButton.YesNo);
              
                if (result == MessageBoxResult.Yes)
                {
                    foreach (ArchitecturalWork a in db.architecturalWorks)
                    {
                        if (a.ArchitectLabel == m.Label)
                        {
                          
                            System.Windows.MessageBox.Show("To delete this architect, you need to delete his works.", "Information", MessageBoxButton.OK);
                            return;
                        }

                    }
                    int idx = 0;
                    foreach (Architect a in db.architects)
                    {
                        if (a.Label == m.Label)
                        {
                            break;
                        }
                        idx++;
                    }
                    db.architects.RemoveAt(idx);
                    db.SaveArchitect();
                    //brisanje sa mape
                    Btn_filter_ClickMap1(null, null);
                    Btn_filter_ClickMap2(null, null);
                    Btn_filter_ClickMap3(null, null);
                    Btn_filter_ClickMap4(null, null);

                    //   architectList.RemoveAt(idx);
                }

            }

        }

        private void Obrisi_arh_work(object sender, RoutedEventArgs e)
        {
            if (dataGrid2.SelectedValue == null)
            {
                System.Windows.MessageBox.Show("There's no selected architectual work");
                return;
            }
            if (dataGrid2.SelectedValue is ArchitecturalWork)
            {
                ArchitecturalWork aw = (ArchitecturalWork)dataGrid2.SelectedValue;
                // upit
                  MessageBoxResult result = System.Windows.MessageBox.Show("Are you sure you want to delete architectural work?", "Delete architetural work", MessageBoxButton.YesNo);



                if (result == MessageBoxResult.Yes)
                {
                    // obrisi

                    int idx = 0;
                    foreach (ArchitecturalWork a in db.architecturalWorks)
                    {
                        if (a.Label == aw.Label)
                        {
                            break;
                        }
                        idx++;
                    }
                    db.architecturalWorks.RemoveAt(idx);
                    db.SaveArchitectWorks();
                    //obrisi sa mape
                    Btn_filter_ClickMap1(null, null);
                    Btn_filter_ClickMap2(null, null);
                    Btn_filter_ClickMap3(null, null);
                    Btn_filter_ClickMap4(null, null);
                    //worksList.RemoveAt(idx);
                }
            }

        }

        private void ArchitectSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGrid1.SelectedValue is Architect)
            {
                Architect m = (Architect)dataGrid1.SelectedValue;
                if (!m.Image.Equals(""))
                    try {
                        ShowIcon.Source = new BitmapImage(new Uri(m.Image));
                    }
                    catch(Exception)
                    {
                        ShowIcon.Source = null;
                    }


            }
            else { ShowIcon.Source = null; }
        }

        private void ShowIconPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (Architect a in ArchitectMaps)
            {
                if (a.Label.Equals(img.Tag))
                {
                    dataGrid1.SelectedValue = a;
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            ShowIcon.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            ShowIcon.Source = null;
                        }
                    }


                }
            }
            if (e.LeftButton == MouseButtonState.Released)
                e.Handled = true;

        }
        private void DraggedImagePreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (Architect a in ArchitectMaps)
            {
                if (a.Label.Equals(img.Tag))
                {
                    dataGrid1.SelectedValue = a;
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            ShowIcon.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            ShowIcon.Source = null;
                        }
                    }

                }
            }
            if (e.LeftButton == MouseButtonState.Released)
                e.Handled = true;

        }

        private void DraggedImagePreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {

            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (Architect a in ArchitectMaps)
            {
                if (a.Label.Equals(img.Tag))
                    dataGrid1.SelectedValue = a;
            }


        }
        private void DraggedImageMouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                Architect selected = (Architect)dataGrid1.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas.Children.Remove(img);
                    DataObject dragData = new DataObject("architect", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }

        }

        private void DraggedImageMouseMove1(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                Architect selected = (Architect)dataGrid1.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas1.Children.Remove(img);
                    DataObject dragData = new DataObject("architect", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void DraggedImageMouseMove2(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                Architect selected = (Architect)dataGrid1.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas2.Children.Remove(img);
                    DataObject dragData = new DataObject("architect", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void DraggedImageMouseMove3(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                Architect selected = (Architect)dataGrid1.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas3.Children.Remove(img);
                    DataObject dragData = new DataObject("architect", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void dropOnMe_Drop(object sender, DragEventArgs e)
        {
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas.Children.Remove(i);

            }
            if (e.Data.GetDataPresent("architect"))
            {
                Architect m = e.Data.GetData("architect") as Architect;

                bool result = false;
                foreach (var c in canvas.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Label)
                        {
                            result = true;
                        }
                    }
                }
                System.Windows.Point d0 = e.GetPosition(canvas);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Label;
                    var positionX = e.GetPosition(canvas).X;
                    var positionY = e.GetPosition(canvas).Y;
                    m.X1 = positionX;
                    m.Y1 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;


                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck1);
                    btn.Tag = m.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.FirstName + " " + m.LastName + "\n" + "Born: " + m.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(m.Died) + "\n" + "BirthPlace: " + m.BirthPlace + "\n" + "Nationality: " + m.Nationality;
                    wp.Children.Add(label);

                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove;
                    ObservableCollection<Architect> listArch = db.architects;
                    int idx = 0;
                    foreach (Architect ma in listArch)
                    {
                        if (ma.Label.Equals(m.Label))
                            break;
                        idx++;
                    }
                    listArch[idx] = m;
                    db.architects = listArch;
                    canvas.Children.Add(img);
                     
                    canvas.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X1 + 20);
                    Canvas.SetTop(btn, m.Y1 - 30);


                    //ShowIcon.Visibility = Visibility.Hidden;
                    //cuvaj ako ne obrise
                    db.SaveArchitect();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);

                    foreach (var c in canvas.Children)
                    {
                        if (c is Line)
                        {
                            // CONTAINS
                            if (((Line)c).Tag.ToString().Contains(img.Tag.ToString()))
                            {
                                ((Line)c).X2 = positionX - 20;
                                ((Line)c).Y2 = positionY - 20;
                            }
                            
                        }
                        if (c is Button && (!((Button)c).Name.Equals("btn_search1") && !((Button)c).Name.Equals("btn_filter1")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }
                    }

                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas.Children)
                    {
                        if (i is Image)
                        {
                            foreach (ArchitecturalWork aw in worksList)
                            {
                                if (((Image)i).Tag.Equals(aw.Name) && aw.ArchitectLabel.Equals(m.Label))
                                {
                                    Line l = new Line();
                                    l.X2 = m.X1 - 20;
                                    l.Y2 = m.Y1 - 20;
                                    l.X1 = Canvas.GetLeft((Image)i);
                                    l.Y1 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = m.Label+ " "+ aw.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                }

            }
            else if (e.Data.GetDataPresent("architectural_work"))
            {

                ArchitecturalWork m = e.Data.GetData("architectural_work") as ArchitecturalWork;


                bool result = false;
                foreach (var c in canvas.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Name)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Name;
                    var positionX = e.GetPosition(canvas).X;
                    var positionY = e.GetPosition(canvas).Y;
                    m.X1 = positionX;
                    m.Y1 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.Name + "\n" + "Place: " + m.Place + "\n" + "Year: " + m.Year + "\n" + "Architect: " + m.Architect
                        + "\n" + "Epoch: " + m.Epoch + "\n" + "Purpose: " + m.Purpose;


                    wp.Children.Add(label);

                    // button za brisanje
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));
                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck5);
                    btn.Tag = m.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove5;
                    ObservableCollection<ArchitecturalWork> listArchWorks = db.architecturalWorks;
                    int idx = 0;
                    foreach (ArchitecturalWork ma in listArchWorks)
                    {
                        if (ma.Name.Equals(m.Name))
                            break;
                        idx++;
                    }
                    listArchWorks[idx] = m;
                    db.architecturalWorks = listArchWorks;
                    canvas.Children.Add(img);
                    canvas.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X1 + 20);
                    Canvas.SetTop(btn, m.Y1 - 30);

                    db.SaveArchitectWorks();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;
                    foreach (var c in canvas.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.Equals(m.ArchitectLabel+ " "+m.Name))
                            {
                                ((Line)c).X1 = positionX - 20;
                                ((Line)c).Y1 = positionY - 20;
                            }
                        }
                        if (c is Button && (!((Button)c).Name.Equals("btn_search1") && !((Button)c).Name.Equals("btn_filter1")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }
                    }

                    // dodati linije ako ih nema
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(m.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = m.X1 - 20;
                                l.Y1 = m.Y1 - 20;
                                l.X2 = Canvas.GetLeft(((Image)i));
                                l.Y2 = Canvas.GetTop(((Image)i));
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = m.ArchitectLabel+" "+m.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas.Children.Add(l);
                    }


                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                }


            }

        }

        private void dropOnMe_Drop2(object sender, DragEventArgs e)
        {
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas1.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas1.Children.Remove(i);

            }

            if (e.Data.GetDataPresent("architect"))
            {
                Architect m = e.Data.GetData("architect") as Architect;

                bool result = false;
                foreach (var c in canvas1.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Label)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas1);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Label;
                    var positionX = e.GetPosition(canvas1).X;
                    var positionY = e.GetPosition(canvas1).Y;
                    m.X2 = positionX;
                    m.Y2 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck2);
                    btn.Tag = m.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;

                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.FirstName + " " + m.LastName + "\n" + "Born: " + m.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(m.Died) + "\n" + "BirthPlace: " + m.BirthPlace + "\n" + "Nationality: " + m.Nationality;
                    wp.Children.Add(label);




                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove1;
                    ObservableCollection<Architect> listArch = db.architects;
                    int idx = 0;
                    foreach (Architect ma in listArch)
                    {
                        if (ma.Label.Equals(m.Label))
                            break;
                        idx++;
                    }
                    listArch[idx] = m;
                    db.architects = listArch;
                    canvas1.Children.Add(img);

                    canvas1.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X2 + 20);
                    Canvas.SetTop(btn, m.Y2 - 30);

                    db.SaveArchitect();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;
                    foreach (var c in canvas1.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.ToString().Contains(img.Tag.ToString()))
                            {
                                ((Line)c).X2 = positionX - 20;
                                ((Line)c).Y2 = positionY - 20;
                            }
                        }


                        if (c is Button && (!((Button)c).Name.Equals("btn_search2") && !((Button)c).Name.Equals("btn_filter2")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }
                        }
                    }
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas1.Children)
                    {
                        if (i is Image)
                        {
                            foreach (ArchitecturalWork aw in worksList)
                            {
                                if (((Image)i).Tag.Equals(aw.Name) && aw.ArchitectLabel.Equals(m.Label))
                                {
                                    Line l = new Line();
                                    l.X2 = m.X2 - 20;
                                    l.Y2 = m.Y2 - 20;
                                    l.X1 = Canvas.GetLeft((Image)i);
                                    l.Y1 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = m.Label+" "+aw.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas1.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                }

            }
            else if (e.Data.GetDataPresent("architectural_work"))
            {

                ArchitecturalWork m = e.Data.GetData("architectural_work") as ArchitecturalWork;

                bool result = false;
                foreach (var c in canvas1.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Name)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas1);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Name;
                    var positionX = e.GetPosition(canvas1).X;
                    var positionY = e.GetPosition(canvas1).Y;
                    m.X2 = positionX;
                    m.Y2 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    // button za brisanje
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));
                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck6);
                    btn.Tag = m.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.Name + "\n" + "Place: " + m.Place + "\n" + "Year: " + m.Year + "\n" + "Architect: " + m.Architect
                         + "\n" + "Epoch: " + m.Epoch + "\n" + "Purpose: " + m.Purpose;
                    wp.Children.Add(label);





                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove6;
                    ObservableCollection<ArchitecturalWork> listArchWorks = db.architecturalWorks;
                    int idx = 0;
                    foreach (ArchitecturalWork ma in listArchWorks)
                    {
                        if (ma.Name.Equals(m.Name))
                            break;
                        idx++;
                    }
                    listArchWorks[idx] = m;
                    db.architecturalWorks = listArchWorks;
                    canvas1.Children.Add(img);
                    canvas1.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X2 + 20);
                    Canvas.SetTop(btn, m.Y2 - 30);
                    db.SaveArchitectWorks();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                    foreach (var c in canvas1.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.Equals(m.ArchitectLabel+" "+m.Name))
                            {
                                ((Line)c).X1 = positionX - 20;
                                ((Line)c).Y1 = positionY - 20;
                            }
                        }

                        if (c is Button && (!((Button)c).Name.Equals("btn_search2") && !((Button)c).Name.Equals("btn_filter2")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }
                    }

                    // dodati linije ako ih nema
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas1.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(m.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = m.X2 - 20;
                                l.Y1 = m.Y2 - 20;
                                l.X2 = Canvas.GetLeft(((Image)i));
                                l.Y2 = Canvas.GetTop(((Image)i));
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = m.ArchitectLabel+" "+m.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas1.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                }


            }

        }

        private void dropOnMe_Drop3(object sender, DragEventArgs e)
        {
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas2.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas2.Children.Remove(i);

            }
            if (e.Data.GetDataPresent("architect"))
            {
                Architect m = e.Data.GetData("architect") as Architect;

                bool result = false;
                foreach (var c in canvas2.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Label)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas2);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Label;
                    var positionX = e.GetPosition(canvas2).X;
                    var positionY = e.GetPosition(canvas2).Y;
                    m.X3 = positionX;
                    m.Y3 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;


                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.FirstName + " " + m.LastName + "\n" + "Born: " + m.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(m.Died) + "\n" + "BirthPlace: " + m.BirthPlace + "\n" + "Nationality: " + m.Nationality;
                    wp.Children.Add(label);

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck3);
                    btn.Tag = m.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;



                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove2;
                    ObservableCollection<Architect> listArch = db.architects;
                    int idx = 0;
                    foreach (Architect ma in listArch)
                    {
                        if (ma.Label.Equals(m.Label))
                            break;
                        idx++;
                    }
                    listArch[idx] = m;
                    db.architects = listArch;
                    canvas2.Children.Add(img);
                    //ShowIcon.Visibility = Visibility.Hidden;
                    //cuvaj ako ne obrise
                    db.SaveArchitect();
                    canvas2.Children.Add(btn);
                    Canvas.SetLeft(btn, positionX + 20);
                    Canvas.SetTop(btn, positionY - 30);

                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                    foreach (var c in canvas2.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.ToString().Contains(img.Tag.ToString()))
                            {
                                ((Line)c).X2 = positionX - 20;
                                ((Line)c).Y2 = positionY - 20;
                            }
                        }
                        if (c is Button && (!((Button)c).Name.Equals("btn_search3") && !((Button)c).Name.Equals("btn_filter3")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }
                        }
                    }
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas2.Children)
                    {
                        if (i is Image)
                        {
                            foreach (ArchitecturalWork aw in worksList)
                            {
                                if (((Image)i).Tag.Equals(aw.Name) && aw.ArchitectLabel.Equals(m.Label))
                                {
                                    Line l = new Line();
                                    l.X2 = m.X3 - 20;
                                    l.Y2 = m.Y3 - 20;
                                    l.X1 = Canvas.GetLeft((Image)i);
                                    l.Y1 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = m.Label+" "+aw.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas2.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                }

            }
            else if (e.Data.GetDataPresent("architectural_work"))
            {

                ArchitecturalWork m = e.Data.GetData("architectural_work") as ArchitecturalWork;

                bool result = false;
                foreach (var c in canvas2.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Name)
                        {
                            result = true;
                        }
                    }
                }
                System.Windows.Point d0 = e.GetPosition(canvas2);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Name;
                    var positionX = e.GetPosition(canvas2).X;
                    var positionY = e.GetPosition(canvas2).Y;
                    m.X3 = positionX;
                    m.Y3 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    // button za brisanje
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));
                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck7);
                    btn.Tag = m.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.Name + "\n" + "Place: " + m.Place + "\n" + "Year: " + m.Year + "\n" + "Architect: " + m.Architect
                         + "\n" + "Epoch: " + m.Epoch + "\n" + "Purpose: " + m.Purpose;
                    wp.Children.Add(label);


                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove7;
                    ObservableCollection<ArchitecturalWork> listArchWorks = db.architecturalWorks;
                    int idx = 0;
                    foreach (ArchitecturalWork ma in listArchWorks)
                    {
                        if (ma.Name.Equals(m.Name))
                            break;
                        idx++;
                    }
                    listArchWorks[idx] = m;
                    db.architecturalWorks = listArchWorks;
                    canvas2.Children.Add(img);
                    canvas2.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X3 + 20);
                    Canvas.SetTop(btn, m.Y3 - 30);

                    db.SaveArchitectWorks();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;
                    foreach (var c in canvas2.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.Equals(m.ArchitectLabel+" "+m.Name))
                            {
                                ((Line)c).X1 = positionX - 20;
                                ((Line)c).Y1 = positionY - 20;
                            }
                        }

                        if (c is Button && (!((Button)c).Name.Equals("btn_search3") && !((Button)c).Name.Equals("btn_filter3")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }

                    }

                    // dodati linije ako ih nema
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas2.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(m.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = m.X3 - 20;
                                l.Y1 = m.Y3 - 20;
                                l.X2 = Canvas.GetLeft(((Image)i));
                                l.Y2 = Canvas.GetTop(((Image)i));
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = m.ArchitectLabel+" "+m.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas2.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added");
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                }


            }


        }

        private void dropOnMe_Drop4(object sender, DragEventArgs e)
        {
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas3.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas3.Children.Remove(i);

            }
            if (e.Data.GetDataPresent("architect"))
            {
                Architect m = e.Data.GetData("architect") as Architect;

                bool result = false;
                foreach (var c in canvas3.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Label)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas3);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Label;
                    var positionX = e.GetPosition(canvas3).X;
                    var positionY = e.GetPosition(canvas3).Y;
                    m.X4 = positionX;
                    m.Y4 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck4);
                    btn.Tag = m.Label;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.FirstName + " " + m.LastName + "\n" + "Born: " + m.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(m.Died) + "\n" + "BirthPlace: " + m.BirthPlace + "\n" + "Nationality: " + m.Nationality;
                    wp.Children.Add(label);

                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                    img.MouseMove += DraggedImageMouseMove3;
                    ObservableCollection<Architect> listArch = db.architects;
                    int idx = 0;
                    foreach (Architect ma in listArch)
                    {
                        if (ma.Label.Equals(m.Label))
                            break;
                        idx++;
                    }
                    listArch[idx] = m;
                    db.architects = listArch;
                    canvas3.Children.Add(img);
                    canvas3.Children.Add(btn);
                    Canvas.SetLeft(btn, positionX + 20);
                    Canvas.SetTop(btn, positionY - 30);
                   
                    db.SaveArchitect();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                    foreach (var c in canvas3.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.ToString().Contains(img.Tag.ToString()))
                            {
                                ((Line)c).X2 = positionX - 20;
                                ((Line)c).Y2 = positionY - 20;
                            }
                        }
                        if (c is Button && (!((Button)c).Name.Equals("btn_search4") && !((Button)c).Name.Equals("btn_filter4")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }
                    }

                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas3.Children)
                    {
                        if (i is Image)
                        {
                            foreach (ArchitecturalWork aw in worksList)
                            {
                                if (((Image)i).Tag.Equals(aw.Name) && aw.ArchitectLabel.Equals(m.Label))
                                {
                                    Line l = new Line();
                                    l.X2 = m.X4 - 20;
                                    l.Y2 = m.Y4 - 20;
                                    l.X1 = Canvas.GetLeft((Image)i);
                                    l.Y1 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = m.Label+" "+aw.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                       
                    }
                    foreach (Line l in lines)
                    {
                        canvas3.Children.Add(l);
                    }

                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid1.SelectedValue = null;
                    ShowIcon.Source = null;

                }

            }
            else if (e.Data.GetDataPresent("architectural_work"))
            {

                ArchitecturalWork m = e.Data.GetData("architectural_work") as ArchitecturalWork;

                bool result = false;
                foreach (var c in canvas3.Children)
                {
                    if (c is Image)
                    {
                        if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == m.Name)
                        {
                            result = true;
                        }
                    }
                }
                //pukne kad se doda search
                System.Windows.Point d0 = e.GetPosition(canvas3);

                if (result == false)
                {
                    Image img = new Image();
                    if (!m.Image.Equals(""))
                    {
                        try
                        {
                            img.Source = new BitmapImage(new Uri(m.Image));
                        }
                        catch (Exception)
                        {
                            img.Source = null;
                        }
                    }

                    img.Width = 50;
                    img.Height = 50;
                    img.Tag = m.Name;
                    var positionX = e.GetPosition(canvas3).X;
                    var positionY = e.GetPosition(canvas3).Y;
                    m.X4 = positionX;
                    m.Y4 = positionY;


                    WrapPanel wp = new WrapPanel();
                    wp.Orientation = Orientation.Vertical;

                    // button za brisanje
                    Image image = new Image();
                    image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));
                    StackPanel stackPnl = new StackPanel();
                    stackPnl.Orientation = Orientation.Horizontal;
                    stackPnl.MaxWidth = 8;
                    stackPnl.MaxHeight = 8;

                    stackPnl.Children.Add(image);

                    Button btn = new Button();
                    btn.Content = stackPnl;
                    btn.Background = Brushes.Transparent;
                    btn.Click += new RoutedEventHandler(this.Button_Clickck8);
                    btn.Tag = m.Name;
                    btn.Style = Resources["MaterialDesignToolButton"] as Style;
                    TextBox label = new TextBox();
                    label.IsEnabled = false;
                    label.Text = "Label: " + m.Label + "\n" + "Name: " + m.Name + "\n" + "Place: " + m.Place + "\n" + "Year: " + m.Year + "\n" + "Architect: " + m.Architect
                         + "\n" + "Epoch: " + m.Epoch + "\n" + "Purpose: " + m.Purpose;
                    wp.Children.Add(label);




                    ToolTip tt = new ToolTip();
                    tt.Content = wp;
                    img.ToolTip = tt;
                    img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                    img.MouseMove += DraggedImageMouseMove8;
                    ObservableCollection<ArchitecturalWork> listArchWorks = db.architecturalWorks;
                    int idx = 0;
                    foreach (ArchitecturalWork ma in listArchWorks)
                    {
                        if (ma.Name.Equals(m.Name))
                            break;
                        idx++;
                    }
                    listArchWorks[idx] = m;
                    db.architecturalWorks = listArchWorks;
                    canvas3.Children.Add(img);
                    canvas3.Children.Add(btn);
                    Canvas.SetLeft(btn, m.X4 + 20);
                    Canvas.SetTop(btn, m.Y4 - 30);
                    db.SaveArchitectWorks();
                    Canvas.SetLeft(img, positionX - 20);
                    Canvas.SetTop(img, positionY - 20);
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                    foreach (var c in canvas3.Children)
                    {
                        if (c is Line)
                        {
                            if (((Line)c).Tag.Equals(m.ArchitectLabel+ " "+m.Name))
                            {
                                ((Line)c).X1 = positionX - 20;
                                ((Line)c).Y1 = positionY - 20;
                            }
                        }
                        if (c is Button && (!((Button)c).Name.Equals("btn_search4") && !((Button)c).Name.Equals("btn_filter4")))
                        {

                            // CONTAINS
                            if (((Button)c).Tag.Equals(img.Tag))
                            {
                                Canvas.SetLeft((Button)c, positionX + 20);
                                Canvas.SetTop((Button)c, positionY - 30);
                            }

                        }
                    }

                    // dodati linije ako ih nema
                    List<Line> lines = new List<Line>();
                    foreach (var i in canvas3.Children)
                    {
                        if (i is Image)
                        {
                            if (((Image)i).Tag.Equals(m.ArchitectLabel))
                            {
                                Line l = new Line();
                                l.X1 = m.X4 - 20;
                                l.Y1 = m.Y4 - 20;
                                l.X2 = Canvas.GetLeft(((Image)i));
                                l.Y2 = Canvas.GetTop(((Image)i));
                                l.Stroke = new SolidColorBrush(Colors.Black);
                                l.StrokeThickness = 1;
                                l.Tag = m.ArchitectLabel+" "+m.Name;

                                lines.Add(l);
                            }
                        }
                    }
                    foreach (Line l in lines)
                    {
                        canvas3.Children.Add(l);
                    }


                }
                else
                {
                    System.Windows.MessageBox.Show("Already added.");
                    dataGrid2.SelectedValue = null;
                    ShowIcon2.Source = null;

                }


            }

        }

        private void DropList_DragEnter(object sender, DragEventArgs e)
        {


            if (!e.Data.GetDataPresent("arhitect") || sender == e.Source)
            {

                e.Effects = DragDropEffects.None;
            }

        }


        private void ShowIconMouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                try
                {
                    Architect selected = (Architect)dataGrid1.SelectedValue;
                    if (selected != null)
                    {
                        DataObject dragData = new DataObject("architect", selected);
                        DragDrop.DoDragDrop(ShowIcon, dragData, DragDropEffects.Move);
                    }
                }
                catch
                {
                    return;
                }
            }

        }

        private void ArchitectWorksSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataGrid2.SelectedValue is ArchitecturalWork)
            {
                ArchitecturalWork aw = (ArchitecturalWork)dataGrid2.SelectedValue;

  
                if (!aw.Image.Equals(""))
                    try
                    {
                        ShowIcon2.Source = new BitmapImage(new Uri(aw.Image));
                    }
                    catch (Exception)
                    {
                        ShowIcon2.Source = null;
                    }
                    
              
            }
            else { ShowIcon2.Source = null; }

        }

        private void ShowIconPreviewMouseLeftButtonDown2(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (ArchitecturalWork a in ArchitectWorks)
            {
                if (a.Name.Equals(img.Tag))
                {
                    dataGrid2.SelectedValue = a;
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            ShowIcon2.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            ShowIcon2.Source = null;
                        }
                    }
                }
            }
            if (e.LeftButton == MouseButtonState.Released)
                e.Handled = true;

        }

        private void ShowIconMouseMove2(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                try
                {
                    ArchitecturalWork selected = (ArchitecturalWork)dataGrid2.SelectedValue;
                    if (selected != null)
                    {
                        DataObject dragData = new DataObject("architectural_work", selected);
                        DragDrop.DoDragDrop(ShowIcon2, dragData, DragDropEffects.Move);
                    }
                }
                catch
                {
                    return;
                }
            }
        }


        private void DraggedImagePreviewMouseLeftButtonDown2(object sender, MouseButtonEventArgs e)
        {

            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (ArchitecturalWork a in ArchitectWorks)
            {
                if (a.Name.Equals(img.Tag))
                {
                    dataGrid2.SelectedValue = a;
                    if (!a.Image.Equals(""))
                    {
                        try
                        {
                            ShowIcon2.Source = new BitmapImage(new Uri(a.Image));
                        }
                        catch (Exception)
                        {
                            ShowIcon2.Source = null;
                        }
                    }

                }
            }
            if (e.LeftButton == MouseButtonState.Released)
                e.Handled = true;

        }

        private void DraggedImagePreviewMouseRightButtonDown2(object sender, MouseButtonEventArgs e)
        {

            startPoint = e.GetPosition(null);
            Image img = sender as Image;

            foreach (ArchitecturalWork a in ArchitectWorks)
            {
                if (a.Name.Equals(img.Tag))
                    dataGrid2.SelectedValue = a;
            }


        }
        private void DraggedImageMouseMove5(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                ArchitecturalWork selected = (ArchitecturalWork)dataGrid2.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas.Children.Remove(img);
                    DataObject dragData = new DataObject("architectural_work", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }

        }

        private void DraggedImageMouseMove6(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                ArchitecturalWork selected = (ArchitecturalWork)dataGrid2.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas1.Children.Remove(img);
                    DataObject dragData = new DataObject("architectural_work", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void DraggedImageMouseMove7(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                ArchitecturalWork selected = (ArchitecturalWork)dataGrid2.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas2.Children.Remove(img);
                    DataObject dragData = new DataObject("architectural_work", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void DraggedImageMouseMove8(object sender, MouseEventArgs e)
        {
            System.Windows.Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
               (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
               Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {


                ArchitecturalWork selected = (ArchitecturalWork)dataGrid2.SelectedValue;

                if (selected != null)
                {
                    Image img = sender as Image;
                    canvas3.Children.Remove(img);
                    DataObject dragData = new DataObject("architectural_work", selected);
                    DragDrop.DoDragDrop(img, dragData, DragDropEffects.Move);

                }

            }
        }

        private void Btn_search_Click(object sender, RoutedEventArgs e)
        {
            if (search5.Text == "Search...")
                search5.Text = "";
            // tekst iz search
            string StringSearch = "";
            ObservableCollection<Architect> resultArch = new ObservableCollection<Architect>();
            foreach (Architect a in db.architects)
            {
                StringSearch = a.Label + a.LastName + a.FirstName + a.BirthPlace + a.Nationality;
                if (StringSearch.ToLower().Contains(this.search5.Text.ToLower()))
                {
                    resultArch.Add(a);
                }
            }
            this.ArchitectMaps = resultArch;
            search5.Text = "Search...";
            search5.Foreground = new SolidColorBrush(Colors.DimGray);

        }

        private void Btn_search_work_Click(object sender, RoutedEventArgs e)
        {
            if (search6.Text == "Search...")
                search6.Text = "";
            // tekst iz search
            string StringSearch = "";
            ObservableCollection<ArchitecturalWork> resultArch_work = new ObservableCollection<ArchitecturalWork>();


            foreach (ArchitecturalWork a in db.architecturalWorks)
            {
                StringSearch = a.Label + a.Name + a.Architect + a.Epoch.ToString() + a.Place + a.Purpose;
                if (StringSearch.ToLower().Contains(this.search6.Text.ToLower()))
                {
                    resultArch_work.Add(a);
                }
            }
            this.ArchitectWorks = resultArch_work;
            search6.Text = "Search...";
            search6.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_refresh_Click(object sender, RoutedEventArgs e)
        {
            db.UpdateBase();
            this.ArchitectWorks = db.architecturalWorks;
            this.ArchitectMaps = db.architects;
        }

        private void Btn_search_ClickMap1(object sender, RoutedEventArgs e)
        {
            
            var search = this.search1.Text;
            this.search1.Text = "";
            Btn_filter_ClickMap1(null, null);
            this.search1.Text = search;
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas.Children.Remove(i);

            }

            //brisanje svih cetvorougla 
            List<Rectangle> listaRec = new List<Rectangle>();
            foreach (var c in canvas.Children)
            {

                string StringSearch = "";
                if (c is Image)
                {

                    foreach (ArchitecturalWork aw in worksList)
                    {

                        StringSearch = aw.Label + aw.Name + aw.Architect + aw.Epoch.ToString() + aw.Place + aw.Purpose;
                        if (((Image)c).Tag.Equals(aw.Name) && StringSearch.ToLower().Contains(this.search1.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }
                    foreach (Architect aw in architectList)
                    {

                        StringSearch = aw.Label + aw.LastName + aw.FirstName + aw.BirthPlace + aw.Nationality;
                        if (((Image)c).Tag.Equals(aw.Label) && StringSearch.ToLower().Contains(this.search1.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }

                }

            }
            foreach (var i in listaRec)
            {
                canvas.Children.Add(i);
            }
            search1.Text = "Search or filter...";
            search1.Foreground = new SolidColorBrush(Colors.DimGray);
            search5.Text = "Search...";
            search5.Foreground = new SolidColorBrush(Colors.DimGray);

        }

        private void Btn_search_ClickMap2(object sender, RoutedEventArgs e)
        {
            var search = this.search2.Text;
            this.search2.Text = "";
            Btn_filter_ClickMap2(null, null);
            this.search2.Text = search;
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas1.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas1.Children.Remove(i);

            }

            //brisanje svih cetvorougla 
            List<Rectangle> listaRec = new List<Rectangle>();
            foreach (var c in canvas1.Children)
            {

                string StringSearch = "";
                if (c is Image)
                {

                    foreach (ArchitecturalWork aw in worksList)
                    {

                        StringSearch = aw.Label + aw.Name + aw.Architect + aw.Epoch.ToString() + aw.Place + aw.Purpose;
                        if (((Image)c).Tag.Equals(aw.Name) && StringSearch.ToLower().Contains(this.search2.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }
                    foreach (Architect aw in architectList)
                    {

                        StringSearch = aw.Label + aw.LastName + aw.FirstName + aw.BirthPlace + aw.Nationality;
                        if (((Image)c).Tag.Equals(aw.Label) && StringSearch.ToLower().Contains(this.search2.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }

                }

            }
            foreach (var i in listaRec)
            {
                canvas1.Children.Add(i);



            }
            search2.Text = "Search or filter...";
            search2.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_search_ClickMap3(object sender, RoutedEventArgs e)
        {
            var search = this.search3.Text;
            this.search3.Text = "";
            Btn_filter_ClickMap3(null, null);
            this.search3.Text = search;
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas2.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas2.Children.Remove(i);

            }

            //brisanje svih cetvorougla 
            List<Rectangle> listaRec = new List<Rectangle>();
            foreach (var c in canvas2.Children)
            {

                string StringSearch = "";
                if (c is Image)
                {

                    foreach (ArchitecturalWork aw in worksList)
                    {

                        StringSearch = aw.Label + aw.Name + aw.Architect + aw.Epoch.ToString() + aw.Place + aw.Purpose;
                        if (((Image)c).Tag.Equals(aw.Name) && StringSearch.ToLower().Contains(this.search3.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }
                    foreach (Architect aw in architectList)
                    {

                        StringSearch = aw.Label + aw.LastName + aw.FirstName + aw.BirthPlace + aw.Nationality;
                        if (((Image)c).Tag.Equals(aw.Label) && StringSearch.ToLower().Contains(this.search3.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }

                }

            }
            foreach (var i in listaRec)
            {
                canvas2.Children.Add(i);



            }
            search3.Text = "Search or filter...";
            search3.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_search_ClickMap4(object sender, RoutedEventArgs e)
        {
            var search = this.search4.Text;
            this.search4.Text = "";
            Btn_filter_ClickMap4(null, null);
            this.search4.Text = search;
            List<Rectangle> cisti = new List<Rectangle>();
            foreach (var c in canvas3.Children)
            {

                if (c is Rectangle)
                {
                    cisti.Add((Rectangle)c);

                }

            }
            foreach (var i in cisti)
            {
                canvas3.Children.Remove(i);

            }

            //brisanje svih cetvorougla 
            List<Rectangle> listaRec = new List<Rectangle>();
            foreach (var c in canvas3.Children)
            {

                string StringSearch = "";
                if (c is Image)
                {

                    foreach (ArchitecturalWork aw in worksList)
                    {

                        StringSearch = aw.Label + aw.Name + aw.Architect + aw.Epoch.ToString() + aw.Place + aw.Purpose;
                        if (((Image)c).Tag.Equals(aw.Name) && StringSearch.ToLower().Contains(this.search4.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }
                    foreach (Architect aw in architectList)
                    {

                        StringSearch = aw.Label + aw.LastName + aw.FirstName + aw.BirthPlace + aw.Nationality;
                        if (((Image)c).Tag.Equals(aw.Label) && StringSearch.ToLower().Contains(this.search4.Text.ToLower()))
                        {
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetValue(Canvas.LeftProperty, Canvas.GetLeft((Image)c));
                            rectangle.SetValue(Canvas.TopProperty, Canvas.GetTop((Image)c));
                            rectangle.Width = ((Image)c).Width;
                            rectangle.Height = ((Image)c).Height;

                            rectangle.Stroke = new SolidColorBrush() { Color = Colors.Yellow };
                            rectangle.StrokeThickness = 3;
                            listaRec.Add(rectangle);


                        }
                    }

                }

            }
            foreach (var i in listaRec)
            {
                canvas3.Children.Add(i);
            }
            search4.Text = "Search or filter...";
            search4.Foreground = new SolidColorBrush(Colors.DimGray);
            search5.Text = "Search...";
            search5.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_filter_ClickMap1(object sender, RoutedEventArgs e)
        {
            if (search1.Text == "Search or filter...")
                search1.Text = "";

            for (int i = canvas.Children.Count - 1; i >= 0; i += -1)
            {
                UIElement Child = canvas.Children[i];
                if (Child is Image || Child is Line || Child is Rectangle)
                {
                    canvas.Children.Remove(Child);
                }
                else if (Child is Button && !((Button)Child).Name.Equals("btn_search1") && !((Button)Child).Name.Equals("btn_filter1"))
                {
                    canvas.Children.Remove(Child);
                }
                
            }


            string StringSearch = "";
            foreach (Architect a in db.architects)
            {
                StringSearch = a.Label + a.LastName + a.FirstName + a.BirthPlace + a.Nationality;
                if (StringSearch.ToLower().Contains(this.search1.Text.ToLower()))
                {

                    bool result = false;
                    foreach (var c in canvas.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;
                    if (a.X1 != -1 || a.Y1 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }


                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Label;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                        wp.Children.Add(label);

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;
                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck1);
                        btn.Tag = a.Label;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                     

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                        img.MouseMove += DraggedImageMouseMove;

                        canvas.Children.Add(img);
                        canvas.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X1 + 20);
                        Canvas.SetTop(btn, a.Y1 - 30);
                        Canvas.SetLeft(img, a.X1 - 20);
                        Canvas.SetTop(img, a.Y1 - 20);
                    }
                }

            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {

                StringSearch = a.Label + a.Name + a.Architect + a.Epoch.ToString() + a.Place + a.Purpose;
                if (StringSearch.ToLower().Contains(this.search1.Text.ToLower()))
                {


                    bool result = false;
                    foreach (var c in canvas.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;
                    if (a.X1 != -1 || a.Y1 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                           
                   

                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Name;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " +a.Architect
                       + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;
                        wp.Children.Add(label);

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;


                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck5);
                        btn.Tag = a.Name;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;


                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                        img.MouseMove += DraggedImageMouseMove5;

                        canvas.Children.Add(img);
                        canvas.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X1 + 20);
                        Canvas.SetTop(btn, a.Y1 - 30);
                        Canvas.SetLeft(img, a.X1 - 20);
                        Canvas.SetTop(img, a.Y1 - 20);

                        List<Line> lines = new List<Line>();
                        foreach (var i in canvas.Children)
                        {
                            if (i is Image)
                            {
                                if (((Image)i).Tag.Equals(a.ArchitectLabel))
                                {
                                    Line l = new Line();
                                    l.X1 = a.X1 - 20;
                                    l.Y1 = a.Y1 - 20;
                                    l.X2 = Canvas.GetLeft((Image)i);
                                    l.Y2 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = a.ArchitectLabel + " "+a.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                        foreach (Line l in lines)
                        {
                            canvas.Children.Add(l);
                        }
                    }

                }
            }
            search1.Text = "Search or filter...";
            search1.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_filter_ClickMap2(object sender, RoutedEventArgs e)
        {
            if (search2.Text == "Search or filter...")
                search2.Text = "";

            for (int i = canvas1.Children.Count - 1; i >= 0; i += -1)
            {
                UIElement Child = canvas1.Children[i];
                if (Child is Image || Child is Line || Child is Rectangle)
                {
                    canvas1.Children.Remove(Child);
                }
                else if (Child is Button && !((Button)Child).Name.Equals("btn_search2") && !((Button)Child).Name.Equals("btn_filter2"))
                {
                    canvas1.Children.Remove(Child);
                }

            }


            string StringSearch = "";
            foreach (Architect a in db.architects)
            {
                StringSearch = a.Label + a.LastName + a.FirstName + a.BirthPlace + a.Nationality;
                if (StringSearch.ToLower().Contains(this.search2.Text.ToLower()))
                {

                    bool result = false;
                    foreach (var c in canvas1.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;
                    if (a.X2 != -1 || a.Y2 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }
                      
                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Label;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;
                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck2);
                        btn.Tag = a.Label;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                        wp.Children.Add(label);

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                        img.MouseMove += DraggedImageMouseMove1;

                        canvas1.Children.Add(img);
                        canvas1.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X2 + 20);
                        Canvas.SetTop(btn, a.Y2 - 30);
                        Canvas.SetLeft(img, a.X2 - 20);
                        Canvas.SetTop(img, a.Y2 - 20);
                    }
                }

            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {

                StringSearch = a.Label + a.Name + a.Architect + a.Epoch.ToString() + a.Place + a.Purpose;
                if (StringSearch.ToLower().Contains(this.search2.Text.ToLower()))
                {


                    bool result = false;
                    foreach (var c in canvas1.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;

                    if (a.X2 != -1 || a.Y2 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }


                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Name;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                         + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;
                        wp.Children.Add(label);


                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;


                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck6);
                        btn.Tag = a.Name;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                        img.MouseMove += DraggedImageMouseMove6;

                        canvas1.Children.Add(img);
                        canvas1.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X2 + 20);
                        Canvas.SetTop(btn, a.Y2 - 30);
                        Canvas.SetLeft(img, a.X2 - 20);
                        Canvas.SetTop(img, a.Y2 - 20);

                        List<Line> lines = new List<Line>();
                        foreach (var i in canvas1.Children)
                        {
                            if (i is Image)
                            {
                                if (((Image)i).Tag.Equals(a.ArchitectLabel))
                                {
                                    Line l = new Line();
                                    l.X1 = a.X2 - 20;
                                    l.Y1 = a.Y2 - 20;
                                    l.X2 = Canvas.GetLeft((Image)i);
                                    l.Y2 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = a.ArchitectLabel +" " +a.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                        foreach (Line l in lines)
                        {
                            canvas1.Children.Add(l);
                        }
                    }
                }
            }
            search2.Text = "Search or filter...";
            search2.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_filter_ClickMap3(object sender, RoutedEventArgs e)
        {
            if (search3.Text == "Search or filter...")
                search3.Text = "";

            for (int i = canvas2.Children.Count - 1; i >= 0; i += -1)
            {
                UIElement Child = canvas2.Children[i];
                if (Child is Image || Child is Line || Child is Rectangle)
                {
                    canvas2.Children.Remove(Child);
                }
                else if (Child is Button && !((Button)Child).Name.Equals("btn_search3") && !((Button)Child).Name.Equals("btn_filter3"))
                {
                    canvas2.Children.Remove(Child);
                }

            }


            string StringSearch = "";
            foreach (Architect a in db.architects)
            {
                StringSearch = a.Label + a.LastName + a.FirstName + a.BirthPlace + a.Nationality;
                if (StringSearch.ToLower().Contains(this.search3.Text.ToLower()))
                {

                    bool result = false;
                    foreach (var c in canvas2.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;
                    if (a.X3 != -1 || a.Y3 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }

                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Label;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;
                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck3);
                        btn.Tag = a.Label;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                        wp.Children.Add(label);

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                        img.MouseMove += DraggedImageMouseMove2;

                        canvas2.Children.Add(img);
                        canvas2.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X3 + 20);
                        Canvas.SetTop(btn, a.Y3- 30);
                        Canvas.SetLeft(img, a.X3 - 20);
                        Canvas.SetTop(img, a.Y3 - 20);
                    }
                }

            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {

                StringSearch = a.Label + a.Name + a.Architect + a.Epoch.ToString() + a.Place + a.Purpose;
                if (StringSearch.ToLower().Contains(this.search3.Text.ToLower()))
                {


                    bool result = false;
                    foreach (var c in canvas2.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;

                    if (a.X3 != -1 || a.Y3 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }

                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Name;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;


                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck7);
                        btn.Tag = a.Name;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                        + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;
                        wp.Children.Add(label);

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                        img.MouseMove += DraggedImageMouseMove7;

                        canvas2.Children.Add(img);
                        canvas2.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X3 + 20);
                        Canvas.SetTop(btn, a.Y3 - 30);
                        Canvas.SetLeft(img, a.X3 - 20);
                        Canvas.SetTop(img, a.Y3 - 20);

                        List<Line> lines = new List<Line>();
                        foreach (var i in canvas2.Children)
                        {
                            if (i is Image)
                            {
                                if (((Image)i).Tag.Equals(a.ArchitectLabel))
                                {
                                    Line l = new Line();
                                    l.X1 = a.X3 - 20;
                                    l.Y1 = a.Y3 - 20;
                                    l.X2 = Canvas.GetLeft((Image)i);
                                    l.Y2 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = a.ArchitectLabel+" "+a.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                        foreach (Line l in lines)
                        {
                            canvas2.Children.Add(l);
                        }
                    }
                }
            }
            search3.Text = "Search or filter...";
            search3.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void Btn_filter_ClickMap4(object sender, RoutedEventArgs e)
        {
            if (search4.Text == "Search or filter...")
                search4.Text = "";

            for (int i = canvas3.Children.Count - 1; i >= 0; i += -1)
            {
                UIElement Child = canvas3.Children[i];
                if (Child is Image || Child is Line || Child is Rectangle)
                {
                    canvas3.Children.Remove(Child);
                }
                else if (Child is Button && !((Button)Child).Name.Equals("btn_search4") && !((Button)Child).Name.Equals("btn_filter4"))
                {
                    canvas3.Children.Remove(Child);
                }
            }


            string StringSearch = "";
            foreach (Architect a in db.architects)
            {
                StringSearch = a.Label + a.LastName + a.FirstName + a.BirthPlace + a.Nationality;
                if (StringSearch.ToLower().Contains(this.search4.Text.ToLower()))
                {

                    bool result = false;
                    foreach (var c in canvas3.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Label)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;
                    if (a.X4 != -1 || a.Y4 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }


                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Label;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;
                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck4);
                        btn.Tag = a.Label;
                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.FirstName + " " + a.LastName + "\n" + "Born: " + a.Born.ToString("MM/dd/yyyy") + "\n" + "Died: " + dc.ConvertInCs(a.Died) + "\n" + "BirthPlace: " + a.BirthPlace + "\n" + "Nationality: " + a.Nationality;
                        wp.Children.Add(label);

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown;
                        img.MouseMove += DraggedImageMouseMove3;

                        canvas3.Children.Add(img);
                        canvas3.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X4 + 20);
                        Canvas.SetTop(btn, a.Y4- 30);
                        Canvas.SetLeft(img, a.X4 - 20);
                        Canvas.SetTop(img, a.Y4 - 20);
                    }
                }

            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {

                StringSearch = a.Label + a.Name + a.Architect + a.Epoch.ToString() + a.Place + a.Purpose;
                if (StringSearch.ToLower().Contains(this.search4.Text.ToLower()))
                {


                    bool result = false;
                    foreach (var c in canvas3.Children)
                    {
                        if (c is Image)
                        {
                            if (((Image)c).Tag != null && ((Image)c).Tag.ToString() == a.Name)
                            {
                                result = true;
                            }
                        }
                    }
                    if (result)
                        continue;

                    if (a.X4 != -1 || a.Y4 != -1)
                    {
                        Image img = new Image();
                        if (!a.Image.Equals(""))
                        {
                            try
                            {
                                img.Source = new BitmapImage(new Uri(a.Image));
                            }
                            catch (Exception)
                            {
                                img.Source = null;
                            }
                        }


                        img.Width = 50;
                        img.Height = 50;
                        img.Tag = a.Name;
                        WrapPanel wp = new WrapPanel();
                        wp.Orientation = Orientation.Vertical;

                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri("images/deldel.png", UriKind.Relative));

                        StackPanel stackPnl = new StackPanel();
                        stackPnl.Orientation = Orientation.Horizontal;


                        stackPnl.MaxWidth = 8;
                        stackPnl.MaxHeight = 8;

                        stackPnl.Children.Add(image);

                        Button btn = new Button();
                        btn.Content = stackPnl;
                        btn.Background = Brushes.Transparent;
                        btn.Click += new RoutedEventHandler(this.Button_Clickck8);
                        btn.Tag = a.Name;

                        btn.Style = Resources["MaterialDesignToolButton"] as Style;
                        TextBox label = new TextBox();
                        label.IsEnabled = false;
                        label.Text = "Label: " + a.Label + "\n" + "Name: " + a.Name + "\n" + "Place: " + a.Place + "\n" + "Year: " + a.Year + "\n" + "Architect: " + a.Architect
                        + "\n" + "Epoch: " + a.Epoch + "\n" + "Purpose: " + a.Purpose;
                        wp.Children.Add(label);

                        ToolTip tt = new ToolTip();
                        tt.Content = wp;
                        img.ToolTip = tt;
                        img.PreviewMouseLeftButtonDown += DraggedImagePreviewMouseLeftButtonDown2;
                        img.MouseMove += DraggedImageMouseMove8;

                        canvas3.Children.Add(img);
                        canvas3.Children.Add(btn);
                        Canvas.SetLeft(btn, a.X4 + 20);
                        Canvas.SetTop(btn, a.Y4 - 30);
                        Canvas.SetLeft(img, a.X4 - 20);
                        Canvas.SetTop(img, a.Y4 - 20);

                        List<Line> lines = new List<Line>();
                        foreach (var i in canvas3.Children)
                        {
                            if (i is Image)
                            {
                                if (((Image)i).Tag.Equals(a.ArchitectLabel))
                                {
                                    Line l = new Line();
                                    l.X1 = a.X4 - 20;
                                    l.Y1 = a.Y4 - 20;
                                    l.X2 = Canvas.GetLeft((Image)i);
                                    l.Y2 = Canvas.GetTop((Image)i);
                                    l.Stroke = new SolidColorBrush(Colors.Black);
                                    l.StrokeThickness = 1;
                                    l.Tag = a.ArchitectLabel+ " "+a.Name;

                                    lines.Add(l);
                                }
                            }
                        }
                        foreach (Line l in lines)
                        {
                            canvas3.Children.Add(l);
                        }
                    }
                }
            }
            search4.Text = "Search or filter...";
            search4.Foreground = new SolidColorBrush(Colors.DimGray);
        }

        private void RemoveTextMap1(object sender, RoutedEventArgs e)
        {
           
            if (search1.Text == "Search or filter...")
            {
                
                search1.Text = "";
                search1.Foreground = new SolidColorBrush(Colors.Black);

            }
        }
        
        private void AddTextMap1(object sender, RoutedEventArgs e)
        {
           
            if (search1.Text == "")
            {
                search1.Text = "Search or filter...";
                search1.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void AddTextMap2(object sender, RoutedEventArgs e)
        {
            if (search2.Text == "")
            {
                search2.Text = "Search or filter...";
                search2.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextMap2(object sender, RoutedEventArgs e)
        {
            if (search2.Text == "Search or filter...")
            {

                search2.Text = "";
                search2.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void AddTextMap3(object sender, RoutedEventArgs e)
        {
            if (search3.Text == "")
            {
                search3.Text = "Search or filter...";
                search3.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextMap3(object sender, RoutedEventArgs e)
        {
            if (search3.Text == "Search or filter...")
            {

                search3.Text = "";
                search3.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void AddTextMap4(object sender, RoutedEventArgs e)
        {
            if (search4.Text == "")
            {
                search4.Text = "Search or filter...";
                search4.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextMap4(object sender, RoutedEventArgs e)
        {
            if (search4.Text == "Search or filter...")
            {

                search4.Text = "";
                search4.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void AddTextMap5(object sender, RoutedEventArgs e)
        {
            if (search5.Text == "")
            {
                search5.Text = "Search...";
                search5.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextMap5(object sender, RoutedEventArgs e)
        {
            if (search5.Text == "Search...")
            {

                search5.Text = "";
                search5.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void AddTextMap6(object sender, RoutedEventArgs e)
        {
            if (search6.Text == "")
            {
                search6.Text = "Search...";
                search6.Foreground = new SolidColorBrush(Colors.DimGray);
            }
        }

        private void RemoveTextMap6(object sender, RoutedEventArgs e)
        {
            if (search6.Text == "Search...")
            {

                search6.Text = "";
                search6.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        public void Button_Clickck1(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;
           
           
            UIElement val = null;
            foreach (var i in canvas.Children) {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val =(UIElement)i;
                    }
                }
            }
           
            foreach (Architect a in db.architects)
            {
                if (a.Label.Equals(btn.Tag))
                {
                    a.X1 = -1;
                    a.Y1 = -1;
                }
            }
            db.SaveArchitect();
            canvas.Children.Remove(btn);
            canvas.Children.Remove(val);
            Btn_filter_ClickMap1(null, null);
        }
        public void Button_Clickck2(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;

           
            UIElement val = null;
            foreach (var i in canvas1.Children)
            {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (Architect a in db.architects)
            {
                if (a.Label.Equals(btn.Tag))
                {
                    a.X2 = -1;
                    a.Y2 = -1;
                }
            }
            db.SaveArchitect();
            canvas1.Children.Remove(btn);
            canvas1.Children.Remove(val);
            Btn_filter_ClickMap2(null, null);
        }
        public void Button_Clickck3(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;

           
            UIElement val = null;
            foreach (var i in canvas2.Children)
            {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (Architect a in db.architects)
            {
                if (a.Label.Equals(btn.Tag))
                {
                    a.X3 = -1;
                    a.Y3 = -1;
                }
            }
            db.SaveArchitect();
            canvas2.Children.Remove(btn);
            canvas2.Children.Remove(val);
            Btn_filter_ClickMap3(null, null);
        }
        public void Button_Clickck4(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;

       
            UIElement val = null;
            foreach (var i in canvas3.Children)
            {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (Architect a in db.architects)
            {
                if (a.Label.Equals(btn.Tag))
                {
                    a.X4 = -1;
                    a.Y4 = -1;
                }
            }
            db.SaveArchitect();
            canvas3.Children.Remove(btn);
            canvas3.Children.Remove(val);
            Btn_filter_ClickMap4(null, null);
        }

        public void Button_Clickck5(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;


            UIElement val = null;
            foreach (var i in canvas.Children)
            {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {
                if (a.Name.Equals(btn.Tag))
                {
                    a.X1 = -1;
                    a.Y1 = -1;
                }
            }
            db.SaveArchitectWorks();
            canvas.Children.Remove(btn);
            canvas.Children.Remove(val);
            Btn_filter_ClickMap1(null, null);
        }
        public void Button_Clickck6(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;

            UIElement val = null;
            foreach (var i in canvas1.Children)
            {
                if (i is Image)
                {
                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {
                if (a.Name.Equals(btn.Tag))
                {
                    a.X2 = -1;
                    a.Y2 = -1;
                }
            }
            db.SaveArchitectWorks();
            canvas1.Children.Remove(btn);
            canvas1.Children.Remove(val);
            Btn_filter_ClickMap2(null, null);
        }
        public void Button_Clickck7(object sender, System.EventArgs e)
        {
            Button btn = sender as Button;

           
            UIElement val = null;
            foreach (var i in canvas2.Children)
            {
                if (i is Image)
                {
                    Console.WriteLine(((Image)i).Tag + "     DASDASDS     ");

                    if (((Image)i).Tag.Equals(btn.Tag))
                    {
                        val = (UIElement)i;
                    }
                }
            }

            foreach (ArchitecturalWork a in db.architecturalWorks)
            {
                if (a.Name.Equals(btn.Tag))
                {
                    a.X3 = -1;
                    a.Y3 = -1;
                }
            }
            db.SaveArchitectWorks();
            canvas2.Children.Remove(btn);
            canvas2.Children.Remove(val);
            Btn_filter_ClickMap3(null, null);
        }
        public void Button_Clickck8(object sender, System.EventArgs e)
            {
                Button btn = sender as Button;

                
                UIElement val = null;
                foreach (var i in canvas3.Children)
                {
                    if (i is Image)
                    {
                        if (((Image)i).Tag.Equals(btn.Tag))
                        {
                            val = (UIElement)i;
                        }
                    }
                }

                foreach (ArchitecturalWork a in db.architecturalWorks)
                {
                    if (a.Name.Equals(btn.Tag))
                    {
                        a.X4 = -1;
                        a.Y4 = -1;
                    }
                }
                db.SaveArchitectWorks();
                canvas3.Children.Remove(btn);
                canvas3.Children.Remove(val);
                Btn_filter_ClickMap4(null, null);

        }

        private void Btn_filter_ClickGrid1(object sender, RoutedEventArgs e)
        {
            if (born1Filter.Text.Equals("") || born1Filter.Text.Equals("From") )
                born1Filter.Text = "0";
            if (born2Filter.Text.Equals("") || born2Filter.Text.Equals("To"))
                born2Filter.Text = "2020";
            if (died1Filter.Text.Equals("") || died1Filter.Text.Equals("From"))
                died1Filter.Text = "0";
            if (died2Filter.Text.Equals("") || died2Filter.Text.Equals("To"))
                died2Filter.Text = "2020";
            string nat = "";
            if (nationality_comboBox.SelectedValue != null)
                nat = nationality_comboBox.SelectedValue.ToString();

            ObservableCollection<Architect> resultArch = new ObservableCollection<Architect>();
            foreach (Architect a in db.architects)
            {
                string born = a.Born.ToString("yyyy");
                string died = a.Died.ToString("yyyy");
                if(Int32.Parse(born) >= Int32.Parse(born1Filter.Text) && Int32.Parse(born) <= Int32.Parse(born2Filter.Text) && a.Nationality.Contains(nat))
                {
                    //0001 - 01 - 01T00: 00:00
                    if (!died.Equals("0001")) 
                    {
                        if(Int32.Parse(died) >= Int32.Parse(died1Filter.Text) && Int32.Parse(died) <= Int32.Parse(died2Filter.Text))
                        {
                            resultArch.Add(a);
                        }
                    }
                    else
                    {
                        resultArch.Add(a);
                    }
                    
                }

            }
            this.ArchitectMaps = resultArch;
            born1Filter.Text = "From";
            born1Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            born2Filter.Text = "To";
            born2Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            died1Filter.Text = "From";
            died1Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            died2Filter.Text = "To";
            died2Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            nationality_comboBox.SelectedValue = null;

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {          
                Regex regex = new Regex("[^0-9]+");
                e.Handled = regex.IsMatch(e.Text);
        }

        private void Btn_filter_ClickGrid2(object sender, RoutedEventArgs e)
        {
            int checked1 = 0;
            int checked2 = 0;

            bool WorldWonder = false;
            bool UnderProtection = false;
            if (WWYes.IsChecked == true)
            {
                WorldWonder = true;
                checked1 = 1;
            }
                
            if (UPYes.IsChecked == true)
            {
                UnderProtection = true;
                checked2 = 1;
            }

            if(WWNo.IsChecked == true)
            {
                WorldWonder = false;
                checked1 = 1;
            }

            if(UPNo.IsChecked == true)
            {
                UnderProtection = false;
                checked2 = 1;
            }
                
            if (year1Filter.Text.Equals("") || year1Filter.Text.Equals("From"))
                year1Filter.Text = "0";
            if (year2Filter.Text.Equals("") || year2Filter.Text.Equals("To"))
                year2Filter.Text = "2020";
            string epoch = "";
            if(epoch_comboBox.SelectedValue != null)
            {
                epoch = epoch_comboBox.SelectedValue.ToString();
            }
            //string epoch = "";
            ObservableCollection<ArchitecturalWork> resultArchWorks = new ObservableCollection<ArchitecturalWork>();
            foreach (ArchitecturalWork a in db.architecturalWorks)
            {

                if (Int32.Parse(a.Year) >= Int32.Parse(year1Filter.Text) && Int32.Parse(a.Year) <= Int32.Parse(year2Filter.Text))
                {
                    if (a.Epoch.ToString().Contains(epoch))
                    {
                        if (checked1 == 1)
                        {
                            if (a.WorldWonder == WorldWonder)
                            {
                                if (checked2 == 1)
                                {
                                    if (a.UnderProtection == UnderProtection)
                                        resultArchWorks.Add(a);
                                }
                                else
                                {
                                    resultArchWorks.Add(a);
                                }
                            }                      
                        }
                        else
                        {
                            if (checked2 == 1)
                            {
                                if (a.UnderProtection == UnderProtection)
                                    resultArchWorks.Add(a);
                            }
                            else
                                resultArchWorks.Add(a);

                        }

                    }
                 
                }

            }
            epoch_comboBox.SelectedValue = null;
            WWNo.IsChecked = false;
            WWYes.IsChecked = false;
            UPNo.IsChecked = false;
            UPYes.IsChecked = false;
            year1Filter.Text = "From";
            year1Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            year2Filter.Text = "To";
            year2Filter.Foreground = new SolidColorBrush(Colors.DimGray);
            this.ArchitectWorks = resultArchWorks;

        }

        private void GotFocusBorn1(object sender, RoutedEventArgs e)
        {
            if (born1Filter.Text == "From")
            {

                born1Filter.Text = "";
                born1Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void LostFocusBorn1(object sender, RoutedEventArgs e)
        {
            if (born1Filter.Text == "")
            {

                born1Filter.Text = "From";
                born1Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void GotFocusBorn2(object sender, RoutedEventArgs e)
        {
            if (born2Filter.Text == "To")
            {

                born2Filter.Text = "";
                born2Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void LostFocusBorn2(object sender, RoutedEventArgs e)
        {
            if (born2Filter.Text == "")
            {

                born2Filter.Text = "To";
                born2Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void GotFocusBorn3(object sender, RoutedEventArgs e)
        {
            if (died1Filter.Text == "From")
            {

                died1Filter.Text = "";
                died1Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void LostFocusBorn3(object sender, RoutedEventArgs e)
        {
            if (died1Filter.Text == "")
            {

                died1Filter.Text = "From";
                died1Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void GotFocusBorn4(object sender, RoutedEventArgs e)
        {
            if (died2Filter.Text == "To")
            {

                died2Filter.Text = "";
                died2Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void LostFocusBorn4(object sender, RoutedEventArgs e)
        {
            if (died2Filter.Text == "")
            {

                died2Filter.Text = "To";
                died2Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void LostFocusBorn5(object sender, RoutedEventArgs e)
        {

            if (year1Filter.Text == "")
            {

                year1Filter.Text = "From";
                year1Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void GotFocusBorn5(object sender, RoutedEventArgs e)
        {
            if (year1Filter.Text == "From")
            {

                year1Filter.Text = "";
                year1Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void LostFocusBorn6(object sender, RoutedEventArgs e)
        {

            if (year2Filter.Text == "")
            {

                year2Filter.Text = "To";
                year2Filter.Foreground = new SolidColorBrush(Colors.DimGray);

            }
        }

        private void GotFocusBorn6(object sender, RoutedEventArgs e)
        {
            if (year2Filter.Text == "To")
            {

                year2Filter.Text = "";
                year2Filter.Foreground = new SolidColorBrush(Colors.Black);

            }
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = HelpProvider.GetHelpKey((DependencyObject)f);
                HelpProvider.ShowHelp(key, this);
            }
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            HelpProvider.ShowHelp("StartPage", this);
        }

        private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void Demo_Click(object sender, RoutedEventArgs e)
        {
            Demo demo = new Demo();
            demo.Show();
        }

        private void DataWindow_Closing(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }

    
}
