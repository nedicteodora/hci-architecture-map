﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ArchitectureMap
{
    /// <summary>
    /// Interaction logic for Demo.xaml
    /// </summary>
    public partial class Demo : Window
    {
        TimeSpan _position;
        public Demo()
        {
            
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
            DataContext = this;
            
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
            DemoPlayer.Play();

        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (DemoPlayer.Source != null)
            {
                if (DemoPlayer.NaturalDuration.HasTimeSpan)
                {
                    lblStatus.Content = String.Format("{0} / {1}", DemoPlayer.Position.ToString(@"mm\:ss"), DemoPlayer.NaturalDuration.TimeSpan.ToString(@"mm\:ss"));
                    sliderSeek.Value = DemoPlayer.Position.Seconds;

                }
                
            }
            else
                lblStatus.Content = "No file selected...";
        }

        private void btnDemoPlay_Click(object sender, RoutedEventArgs e)
        {
            DemoPlayer.Play();
        }

        private void btnDemoPause_Click(object sender, RoutedEventArgs e)
        {
            DemoPlayer.Pause();
        }

        private void btnDemoStop_Click(object sender, RoutedEventArgs e)
        {
            DemoPlayer.Stop();
        }

        private void DemoPlayer_MediaOpened(object sender, RoutedEventArgs e)
        {
            _position = DemoPlayer.NaturalDuration.TimeSpan;
            sliderSeek.Minimum = 0;
            sliderSeek.Maximum = _position.TotalSeconds;
        }


        private void sliderSeek_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            int pos = Convert.ToInt32(sliderSeek.Value);
            DemoPlayer.Position = new TimeSpan(0, 0, 0, pos, 0);
        }

        private void sliderSeek_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (sliderSeek.IsMouseCaptureWithin)
            {
                int pos = Convert.ToInt32(sliderSeek.Value);
                DemoPlayer.Position = new TimeSpan(0, 0, 0, pos, 0);
            }
        }
    }
}
